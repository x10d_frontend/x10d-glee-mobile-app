module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['./src'],
        extensions: ['.js', '.ts', '.tsx', '.json'],
        alias: {
          '@features': './src/features',
          '@entities': './src/entities',
          '@pages': './src/pages',
          '@shared': './src/shared',
        },
      },
    ],
    ['@babel/plugin-proposal-optional-catch-binding'],
  ],
};
