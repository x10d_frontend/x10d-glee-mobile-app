import { AppRegistry } from 'react-native';
import App from './src/app';

AppRegistry.registerComponent('Glee', () => App);
export default App;
