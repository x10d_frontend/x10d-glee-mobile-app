const shell = require('shelljs');

shell.config.fatal = true;

function main() {
  const { ANDROID_HOME } = process.env;

  if (process.platform === 'win32') {
    shell.exec(`${ANDROID_HOME}/emulator/emulator -avd Pixel_5_API_28`);
  }
}

main();
