import * as React from 'react';
import { Pressable, View } from 'react-native';

import { Text, Icon } from '@shared/ui';

import { styles } from './styles';

export interface InputNumberProps {
  value: number;

  onChangeValue: (value: number) => void;
}

export const InputNumber = function InputNumber(props: InputNumberProps) {
  return (
    <View style={styles.container}>
      <Pressable
        style={styles.button}
        onPress={() => props.onChangeValue(props.value - 1)}
      >
        <Icon icon="minus" />
      </Pressable>

      <Text
        text={props.value.toString(10)}
        style={styles.value}
        preset="body_l"
      />

      <Pressable
        style={styles.button}
        hitSlop={{
          top: 20,
          right: 20,
          bottom: 20,
          left: 20,
        }}
        onPress={() => props.onChangeValue(props.value + 1)}
      >
        <Icon icon="plus-small" />
      </Pressable>
    </View>
  );
};
