import { StyleSheet } from 'react-native';

import { deviceWidth } from '@shared/lib/dimensions';
import { color, spacing } from '@shared/ui';

export const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: deviceWidth * 0.36,
    height: 40,
    borderRadius: 10,
    backgroundColor: color.palette.blackcurrant,
  },

  button: {
    paddingHorizontal: spacing[4],
  },

  value: {
    fontWeight: '900',
    fontSize: 19,
  },
});
