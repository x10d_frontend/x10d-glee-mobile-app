import * as React from 'react';
import { Pressable } from 'react-native';
import { Text } from '@shared/ui';
import { buttonViewPresets, buttonTextPresets } from './button.presets';
import { ButtonProps } from './button.props';

export function Button(props: ButtonProps) {
  const {
    preset = 'primary',
    tx,
    text,
    style: styleOverride,
    textStyle: textStyleOverride,
    children,
    disabled,
    ...rest
  } = props;

  const viewStyle = buttonViewPresets[preset] || buttonViewPresets.primary;
  const viewStyleDisabled = disabled
    ? buttonViewPresets[`${preset}_disabled`]
    : null;
  const viewStyles = [viewStyle, viewStyleDisabled, styleOverride];
  const textStyle = buttonTextPresets[preset] || buttonTextPresets.primary;
  const textStyleDisabled = disabled
    ? buttonTextPresets[`${preset}_disabled`]
    : null;
  const textStyles = [textStyle, textStyleDisabled, textStyleOverride];

  return (
    <Pressable
      disabled={disabled}
      style={viewStyles}
      hitSlop={{
        top: 20,
        right: 20,
        bottom: 20,
        left: 20,
      }}
      {...rest}
    >
      {(tx || text) && <Text tx={tx} text={text} style={textStyles} />}

      {children}
    </Pressable>
  );
}

export { buttonTextPresets };
