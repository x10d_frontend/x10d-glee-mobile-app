import React from 'react';
import {
  StyleProp,
  TextStyle,
  TouchableOpacityProps,
  ViewStyle,
} from 'react-native';

import { TxKeyPath } from '@shared/lib/i18n';

import { ButtonPresetNames } from './button.presets';

export interface ButtonProps extends TouchableOpacityProps {
  tx?: TxKeyPath;

  text?: string;

  style?: StyleProp<ViewStyle>;

  textStyle?: StyleProp<TextStyle>;

  preset?: ButtonPresetNames;

  children?: React.ReactNode;
}
