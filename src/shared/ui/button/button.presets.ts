import { ViewStyle, TextStyle } from 'react-native';

import { color, spacing, mainButtonHeight } from '../theme';
import { textPresets } from '../text';

const BASE_VIEW: ViewStyle = {
  justifyContent: 'center',
  alignItems: 'center',
  height: mainButtonHeight,
  paddingHorizontal: spacing[6],
  borderRadius: spacing[3],
};

const PINK_VIEW: ViewStyle = {
  backgroundColor: color.palette.cerise,
  borderRadius: 40,
};

const DARK_VIEW: ViewStyle = {
  backgroundColor: color.palette.mulledWineDarken,
};

const BASE_TEXT: TextStyle = {
  textAlign: 'center',
  paddingHorizontal: spacing[3],
};

const DARK_TEXT: TextStyle = {
  ...BASE_TEXT,
  ...textPresets.body_l,
  fontWeight: '900',
};

export const buttonViewPresets: Record<string, ViewStyle> = {
  primary: {
    ...BASE_VIEW,
    backgroundColor: color.palette.cerise,
    borderRadius: 40,
  },

  pink: {
    ...BASE_VIEW,
    ...PINK_VIEW,
  },

  pink_disabled: {
    ...BASE_VIEW,
    ...PINK_VIEW,
    backgroundColor: color.palette.blackcurrant,
  },

  outline: {
    ...BASE_VIEW,
    backgroundColor: color.transparent,
    borderWidth: 2,
    borderColor: color.palette.blackcurrant,
  },

  dark: {
    ...BASE_VIEW,
    ...DARK_VIEW,
  },

  dark_rounded: {
    ...BASE_VIEW,
    ...DARK_VIEW,
    width: mainButtonHeight,
    borderRadius: mainButtonHeight / 2,
  },

  dark_disabled: {
    ...BASE_VIEW,
    ...DARK_VIEW,
  },

  link: {
    ...BASE_VIEW,
    paddingHorizontal: 0,
    paddingVertical: 0,
    alignItems: 'flex-start',
  },
};

export const buttonTextPresets: Record<ButtonPresetNames, TextStyle> = {
  primary: textPresets.header,
  pink: textPresets.header,
  pink_disabled: {
    ...textPresets.header,
    color: color.palette.mulledWine,
  },
  outline: textPresets.body,
  dark: DARK_TEXT,
  dark_disabled: {
    ...DARK_TEXT,
    color: color.palette.mobster,
  },
  link: {
    ...BASE_TEXT,
    color: color.text,
    paddingHorizontal: 0,
    paddingVertical: 0,
  } as TextStyle,
};

/**
 * A list of preset names.
 */
export type ButtonPresetNames = keyof typeof buttonViewPresets;
