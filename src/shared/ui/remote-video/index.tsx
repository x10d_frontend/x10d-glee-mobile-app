import React, { useRef, useState } from 'react';
import { Animated, StyleProp, StyleSheet, View, ViewStyle } from 'react-native';
import ContentLoader, { Rect } from 'react-content-loader/native';
import { ResizeMode, Video, VideoProps } from 'expo-av';

import { color } from '@shared/ui';

interface RemoteVideoSkeletonProps {
  width: number;
  height: number;
  style?: StyleProp<ViewStyle>;
}

const AnimatedVideo = Animated.createAnimatedComponent(Video);

export interface RemoteVideoProps extends VideoProps {
  width: number;
  height: number;
  style: StyleProp<any>;
}

export const RemoteVideoSkeleton: React.FC<
  RemoteVideoSkeletonProps
> = props => (
  <ContentLoader
    style={props.style}
    width={props.width}
    height={props.height}
    backgroundColor={color.palette.mulledWine}
    foregroundColor={color.palette.mulledWineDarken}
  >
    <Rect width={props.width} height={props.height} />
  </ContentLoader>
);

export const RemoteVideo = function RemoteVideo(props: RemoteVideoProps) {
  const [wasVideoLoaded, setVideoLoaded] = useState(false);
  const videoOpacity = useRef(new Animated.Value(0)).current;
  const handleLoad = async () => {
    setVideoLoaded(true);

    Animated.timing(videoOpacity, {
      toValue: 1,
      duration: 800,
      useNativeDriver: true,
    }).start();
  };

  const { style, width, height, source, ...restProps } = props;

  if (source === undefined) {
    return <RemoteVideoSkeleton width={width} height={height} />;
  }

  return (
    <View>
      <AnimatedVideo
        source={source}
        style={[style, { width, height }, { opacity: videoOpacity }]}
        onLoad={handleLoad}
        usePoster={false}
        isMuted
        isLooping
        shouldPlay={false}
        resizeMode={ResizeMode.COVER}
        {...restProps}
      />

      {!wasVideoLoaded && (
        <RemoteVideoSkeleton
          style={StyleSheet.absoluteFill}
          width={width}
          height={height}
        />
      )}
    </View>
  );
};
