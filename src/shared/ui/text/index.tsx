import * as React from 'react';
import { Text as ReactNativeText } from 'react-native';

import { translate } from '@shared/lib/i18n';

import { textPresets } from './text.presets';
import { TextProps } from './text.props';

export function Text(props: TextProps) {
  const {
    preset = 'default',
    tx,
    txOptions,
    text,
    children,
    style: styleOverride,
    ...rest
  } = props;

  const i18nText = tx && translate(tx, txOptions);
  const content = i18nText || text || children;

  const style = textPresets[preset] || textPresets.default;
  const styles = [style, styleOverride];

  return (
    <ReactNativeText {...rest} style={styles}>
      {content}
    </ReactNativeText>
  );
}

export { textPresets };
