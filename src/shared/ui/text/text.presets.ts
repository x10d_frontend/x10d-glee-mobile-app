import { TextStyle } from 'react-native';

import { color, typography } from '@shared/ui/theme';

const BASE: TextStyle = {
  fontFamily: typography.primary,
  color: color.text,
  fontSize: 15,
};

const BODY: TextStyle = {
  fontSize: 18,
  lineHeight: 25,
  fontWeight: '400',
};

export const textPresets = {
  default: BASE,

  secondary: {
    ...BASE,
    fontSize: 16,
    lineHeight: 25,
    color: color.palette.mobster,
  },

  bold: { ...BASE, fontWeight: 'bold' } as TextStyle,

  header: {
    ...BASE,
    fontSize: 32,
    fontWeight: '900',
    lineHeight: 37,
  } as TextStyle,

  subHeader: {
    ...BASE,
    fontSize: 17,
    fontWeight: '500',
    lineHeight: 20,
    color: color.secondaryText,
  } as TextStyle,

  body: {
    ...BASE,
    ...BODY,
  },

  body_l: {
    ...BASE,
    ...BODY,
    fontWeight: '500',
  } as TextStyle,
};

export type TextPresets = keyof typeof textPresets;
