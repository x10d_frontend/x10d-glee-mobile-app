import React from 'react';
import {
  StyleProp,
  TextProps as TextProperties,
  TextStyle,
} from 'react-native';
import i18n from 'i18n-js';

import { TxKeyPath } from '@shared/lib/i18n';

import { TextPresets } from './text.presets';

export interface TextProps extends TextProperties {
  children?: React.ReactNode;

  tx?: TxKeyPath;

  txOptions?: i18n.TranslateOptions;

  text?: string;

  style?: StyleProp<TextStyle>;

  preset?: TextPresets;
}
