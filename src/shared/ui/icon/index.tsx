import React from 'react';
import { View, StyleSheet } from 'react-native';

import { AutoImage as Image } from '@shared/ui/auto-image';

import { IconProps } from './icon.props';
import { icons } from './icons';

const styles = StyleSheet.create({
  icon: {
    resizeMode: 'contain',
  },
});

export function Icon(props: IconProps) {
  const { style: styleOverride, icon, containerStyle } = props;

  return (
    <View style={containerStyle}>
      <Image style={[styles.icon, styleOverride]} source={icons[icon]} />
    </View>
  );
}
