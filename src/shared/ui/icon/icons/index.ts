export const icons = {
  chevron: require('./chevron.png'),
  clock: require('./clock.png'),
  edit: require('./edit.png'),
  personal: require('./personal.png'),
  plus: require('./plus.png'),
  templates: require('./templates.png'),
  trash: require('./trash.png'),
  chain: require('./chain.png'),
  minus: require('./minus.png'),
  'plus-small': require('./plus-small.png'),
};

export type IconTypes = keyof typeof icons;
