import * as React from 'react';
import { StyleSheet } from 'react-native';
import { LinearGradient, LinearGradientProps } from 'expo-linear-gradient';

export function GradientBackground(props: LinearGradientProps) {
  return <LinearGradient {...props} style={StyleSheet.absoluteFill} />;
}
