import React from 'react';
import { View } from 'react-native';

import { GradientBackground, RemoteVideo, RemoteVideoProps } from '@shared/ui';
import { deviceHeight, deviceWidth } from '@shared/lib/dimensions';

interface IScreenHeaderProps
  extends Omit<RemoteVideoProps, 'height' | 'width'> {
  height?: number;
}

export const ScreenHeader = function ScreenHeader(props: IScreenHeaderProps) {
  const { source, style, height } = props;
  const actualHeight = height ?? deviceHeight * 0.48;

  return (
    <View>
      <RemoteVideo
        width={deviceWidth}
        height={actualHeight}
        style={style}
        source={source}
        shouldPlay
      />

      <GradientBackground colors={['#09011000', '#090110']} />
    </View>
  );
};
