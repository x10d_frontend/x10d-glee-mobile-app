import React from 'react';

import { Screen, ScreenProps } from '@shared/ui';
import { useScalingOnScroll } from '@shared/lib/scaling-on-scroll';

import { ScreenHeader } from './screen-header';

interface IScreenWithVideoHeaderProps extends ScreenProps {
  videoSource: any;
  headerHeight?: number;
}

export const ScreenWithVideoHeader: React.FC<
  IScreenWithVideoHeaderProps
> = props => {
  const { scrollHandler, scalingStyles } = useScalingOnScroll();

  const { videoSource, headerHeight, ...restProps } = props;

  return (
    <Screen
      header={
        <ScreenHeader
          style={scalingStyles}
          source={videoSource}
          height={headerHeight}
        />
      }
      scrollViewProps={{
        scrollEventThrottle: 16,
        onScroll: scrollHandler,
      }}
      {...restProps}
    >
      {props.children}
    </Screen>
  );
};
