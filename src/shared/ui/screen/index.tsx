import * as React from 'react';
import {
  Animated,
  KeyboardAvoidingView,
  ScrollViewProps,
  StatusBar,
  StyleProp,
  View,
  ViewStyle,
} from 'react-native';

import { styles } from './styles';

export interface ScreenProps {
  header?: React.ReactNode;
  children?: React.ReactNode;

  statusBar?: 'light-content' | 'dark-content';

  scrollViewProps?: ScrollViewProps;

  style?: StyleProp<ViewStyle>;
  contentStyle?: StyleProp<ViewStyle>;
}

export function Screen(props: ScreenProps) {
  return (
    <KeyboardAvoidingView style={styles.container} behavior="padding">
      <StatusBar barStyle={props.statusBar || 'light-content'} />

      <Animated.ScrollView
        style={styles.container}
        contentContainerStyle={props.style}
        keyboardShouldPersistTaps="handled"
        showsVerticalScrollIndicator={false}
        {...props.scrollViewProps}
      >
        {props.header}

        <View style={[styles.content, props.contentStyle]}>
          {props.children}
        </View>
      </Animated.ScrollView>
    </KeyboardAvoidingView>
  );
}
