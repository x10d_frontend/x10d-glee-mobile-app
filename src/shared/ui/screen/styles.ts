import { StyleSheet } from 'react-native';

import { color, mainSidePadding } from '@shared/ui';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: '100%',
    backgroundColor: color.background,
  },

  content: {
    backgroundColor: color.background,
    paddingHorizontal: mainSidePadding,
    paddingBottom: mainSidePadding,
  },
});
