import { StyleSheet } from 'react-native';

import { color, spacing, mainButtonHeight } from '../theme';
import { buttonTextPresets } from '../button';

export const styles = StyleSheet.create({
  button: {
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    flexBasis: '50%',
    paddingHorizontal: spacing[5],
  },

  button_add: {
    borderBottomLeftRadius: 0,
    borderLeftColor: color.palette.mulledWine,
    borderLeftWidth: 1,
  },

  button_cancel: {
    borderBottomRightRadius: 0,
  },

  list: {
    justifyContent: 'center',
  },
  listAddItem__buttons: {
    flexDirection: 'row',
  },
  listAddItem__input: {
    backgroundColor: color.palette.blackcurrant,
    borderTopLeftRadius: spacing[3],
    borderTopRightRadius: spacing[3],
    height: mainButtonHeight,
    textAlign: 'center',
    ...buttonTextPresets.dark,
  },
  listItemAction__icon: {
    justifyContent: 'center',
  },

  listItem__action: {
    marginRight: spacing[3],
  },

  listItem__actions: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },

  list__addItem: {
    marginTop: spacing[3],
  },

  list__item: {
    marginTop: spacing[3],
  },
});
