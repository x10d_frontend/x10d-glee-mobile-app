import React, { useState } from 'react';
import {
  StyleProp,
  View,
  ViewStyle,
  TextInput,
  TextInputProps,
} from 'react-native';
import { SwipeRow } from 'react-native-swipe-list-view';

import { Button, Icon, color, mainButtonHeight } from '@shared/ui';

import { styles } from './styles';

export interface EditableListProps {
  items: Array<string>;

  onAddItem: (item: string) => void;

  onEditItem: (item: string) => void;

  onDeleteItem: (item: string) => void;

  onAddButtonPress: () => void;

  isAddingItemsAllowed?: boolean;

  textInputProps?: TextInputProps;

  style?: StyleProp<ViewStyle>;
}

export const EditableList = function EditableList(props: EditableListProps) {
  const [isAddItemShown, setIsAddItemShown] = useState<boolean>(false);
  const [newItemValue, setNewItemValue] = useState<string>('');
  const [editItemValue, setEditItemValue] = useState<string | null>(null);

  const isAddItemDisabled = newItemValue === '';

  const ITEM_OPEN_VALUE =
    -(mainButtonHeight + (styles.listItem__action.marginRight as number)) * 2;

  const showAddItem = () => {
    props.onAddButtonPress();

    setIsAddItemShown(true);
  };

  const addItem = () => {
    if (newItemValue !== '') {
      props.onAddItem(newItemValue);

      setNewItemValue('');
    }

    setIsAddItemShown(false);
  };

  const cancelAddingItem = () => {
    if (editItemValue !== null) {
      setNewItemValue(editItemValue);
      addItem();
    }

    setIsAddItemShown(false);
  };

  const editItem = (item: string) => {
    setEditItemValue(item);
    props.onEditItem(item);

    setNewItemValue(item);
    setIsAddItemShown(true);
  };

  const deleteItem = (item: string) => {
    props.onDeleteItem(item);
  };

  return (
    <View style={[styles.list, props.style]}>
      {props.isAddingItemsAllowed && (
        <Button
          disabled={isAddItemShown}
          onPress={showAddItem}
          preset="outline"
        >
          <Icon icon="plus" />
        </Button>
      )}

      {isAddItemShown && (
        <View style={styles.list__addItem}>
          <TextInput
            {...props.textInputProps}
            value={newItemValue}
            onChangeText={setNewItemValue}
            onSubmitEditing={addItem}
            style={styles.listAddItem__input}
            autoFocus
            returnKeyType="done"
            selectionColor={color.palette.white}
            placeholderTextColor={color.palette.mobster}
          />

          <View style={styles.listAddItem__buttons}>
            <Button
              style={[styles.button, styles.button_cancel]}
              onPress={cancelAddingItem}
              tx="buttons.cancel"
              preset="dark"
            />

            <Button
              style={[styles.button, styles.button_add]}
              disabled={isAddItemDisabled}
              onPress={addItem}
              tx="buttons.ok"
              preset="dark"
            />
          </View>
        </View>
      )}

      {props.items.map(option => (
        // @ts-expect-error. Неверные типы в библиотеке
        <SwipeRow
          key={option}
          swipeToOpenPercent={20}
          swipeToClosePercent={20}
          rightOpenValue={ITEM_OPEN_VALUE}
          disableRightSwipe
          style={styles.list__item}
        >
          <View style={styles.listItem__actions}>
            <Button
              style={styles.listItem__action}
              onPress={() => editItem(option)}
              preset="dark_rounded"
            >
              <Icon containerStyle={styles.listItemAction__icon} icon="edit" />
            </Button>

            <Button onPress={() => deleteItem(option)} preset="dark_rounded">
              <Icon containerStyle={styles.listItemAction__icon} icon="trash" />
            </Button>
          </View>

          <Button text={option} preset="dark" />
        </SwipeRow>
      ))}
    </View>
  );
};
