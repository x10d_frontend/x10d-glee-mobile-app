export const sortTuplesBySecondElement = (
  [_, valueA]: [string, number],
  [__, valueB]: [string, number],
): number => valueB - valueA;

export const transformArrayToMap = <T extends { id: string }>(
  array: Array<T>,
) => Object.fromEntries(array.map(item => [item.id, item]));
