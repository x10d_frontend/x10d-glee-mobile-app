import { useRef } from 'react';
import { Animated } from 'react-native';

// https://mariusreimer.com/blog/id/rn-image-scroll-zoom
export const useScalingOnScroll = () => {
  const offset = useRef(new Animated.ValueXY()).current;

  return {
    scalingStyles: {
      transform: [
        {
          translateY: offset.y.interpolate({
            inputRange: [-1000, 0],
            outputRange: [-100, 0],
            extrapolate: 'clamp',
          }),
        },
        {
          scale: offset.y.interpolate({
            inputRange: [-3000, 0],
            outputRange: [20, 1],
            extrapolate: 'clamp',
          }),
        },
      ],
    },
    scrollHandler: Animated.event(
      [{ nativeEvent: { contentOffset: { y: offset.y } } }],
      {
        useNativeDriver: true,
      },
    ),
  };
};
