import * as Localization from 'expo-localization';
import i18n from 'i18n-js';

import ru from './ru.json';
import en from './en.json';

const defaultLocale = 'ru';
const locale = Localization.locale.split('-')[0] || defaultLocale;

i18n.fallbacks = true;
i18n.translations = { ru, en };
i18n.defaultLocale = defaultLocale;
i18n.locale = locale;

type DefaultLocale = typeof ru;

type RecursiveKeyOf<TObj extends Record<string, any>> = {
  [TKey in keyof TObj & string]: TObj[TKey] extends Record<string, any>
    ? // @ts-expect-error
      `${TKey}` | `${TKey}.${RecursiveKeyOf<TObj[TKey]>}`
    : `${TKey}`;
}[keyof TObj & string];

export type TxKeyPath = RecursiveKeyOf<DefaultLocale>;

export function translate(key: TxKeyPath, options?: i18n.TranslateOptions) {
  return i18n.t(key, options);
}

export interface ITranslatableEntity {
  ru: string;
  en: string;

  [key: string]: string;
}

export function translateEntity(entity: ITranslatableEntity) {
  return entity[i18n.locale];
}
