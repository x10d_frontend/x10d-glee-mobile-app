import { ITemplate } from '@shared/services/repository';

import { AnalyticsTemplateType, ITemplateEventPayload } from './types';

export const events = {
  TEMPLATE_CARD_WAS_SHOWN_EVENT_NAME: 'template_card_shown',
  TEMPLATE_CARD_WAS_PRESSED_EVENT_NAME: 'template_card_pressed',
  TEMPLATES_FILTER_WAS_PRESSED_EVENT_NAME: 'templates_filter_pressed',
  TEMPLATE_WAS_CLOSED_EVENT_NAME: 'template_closed',
  TEMPLATE_WAS_STARTED_EVENT_NAME: 'template_started',
  TEMPLATE_VIDEO_AUTHOR_WAS_PRESSED_EVENT_NAME: 'template_video_author_opened',
  NEW_OPTION_WAS_PRESSED_EVENT_NAME: 'new_comparison_option_pressed',
  NEW_OPTION_WAS_ADDED_EVENT_NAME: 'new_comparison_option_added',
  OPTION_WAS_RENAMED_EVENT_NAME: 'comparison_option_renamed',
  OPTION_WAS_DELETED_EVENT_NAME: 'comparison_option_deleted',
  NEW_CRITERION_WAS_PRESSED_EVENT_NAME: 'new_comparison_criterion_pressed',
  NEW_CRITERION_WAS_ADDED_EVENT_NAME: 'new_comparison_criterion_added',
  CRITERION_WAS_RENAMED_EVENT_NAME: 'comparison_criterion_renamed',
  CRITERION_WAS_DELETED_EVENT_NAME: 'comparison_criterion_deleted',
  CRITERION_WEIGHT_WAS_CHANGED_EVENT_NAME:
    'comparison_criterion_weight_changed',
  DECISION_WAS_MADE_EVENT_NAME: 'template_finished',
  DECISION_WAS_ALMOST_VIEWED_EVENT_NAME: 'template_result_75%_read',

  NEXT_STEP_BUTTON_WAS_PRESSED_EVENT_NAME: 'next_step_button_pressed',
  PREVIOUS_STEP_BUTTON_WAS_PRESSED_EVENT_NAME: 'previous_step_button_pressed',
};

export const getAnalyticsTemplateType = (
  template?: ITemplate,
): AnalyticsTemplateType => {
  if (template === undefined) {
    return AnalyticsTemplateType.CUSTOM;
  }

  return template.featured
    ? AnalyticsTemplateType.FEATURED
    : AnalyticsTemplateType.COMMON;
};

export const getTemplateEventPayload = (
  template?: ITemplate,
): ITemplateEventPayload => ({
  templateId: template?.id ?? null,
  templateName: template?.title.ru ?? null,
  templateType: getAnalyticsTemplateType(template),
});

export const getTemplateFilterEventPayload = (filter: string | null) => ({
  filterId: filter,
});
