import { ITemplate } from '@shared/services/repository';

export enum AnalyticsTemplateType {
  FEATURED = 'FEATURED',
  COMMON = 'COMMON',
  CUSTOM = 'CUSTOM',
}

export interface ILogEventPayload extends Record<string, any> {
  name: string;
}

export interface IEventBasePayload {
  step: number;
  template?: ITemplate;
}

export interface IOptionEventPayload extends IEventBasePayload {
  option: string;
}

export interface ICriterionEventPayload extends IEventBasePayload {
  criterion: string;
}

export interface ITemplateEventPayload {
  templateName: string | null;
  templateId: string | null;
  templateType: AnalyticsTemplateType;
}

export interface ITemplateFilterEventPayload {
  filterName: string;
  filterId: string;
}
