import { Mixpanel } from 'mixpanel-react-native';
import { createEffect, attach } from 'effector';
import { FirebaseAuthTypes } from '@react-native-firebase/auth';

import { ITemplate } from '@shared/services/repository';

import {
  events,
  getTemplateEventPayload,
  getTemplateFilterEventPayload,
} from './lib';
import type {
  IEventBasePayload,
  ILogEventPayload,
  IOptionEventPayload,
  ICriterionEventPayload,
} from './types';

const instance = new Mixpanel('b2668563ce9b24421c02963bd2282c0c');

const logEventFx = createEffect(
  async ({ name, ...restPayload }: ILogEventPayload) => {
    await instance.track(name, restPayload);
  },
);

export const initializeFx = createEffect(
  async (user: FirebaseAuthTypes.User) => {
    await instance.init();

    await instance.registerSuperProperties({
      userId: user.uid,
    });
  },
);

export const sendTemplateCardWasPressedEventFx = attach({
  effect: logEventFx,
  mapParams: (template: ITemplate) => ({
    name: events.TEMPLATE_CARD_WAS_PRESSED_EVENT_NAME,
    ...getTemplateEventPayload(template),
  }),
});

export const sendTemplateCardWasShownEventFx = attach({
  effect: logEventFx,
  mapParams: (template: ITemplate) => ({
    name: events.TEMPLATE_CARD_WAS_SHOWN_EVENT_NAME,
    ...getTemplateEventPayload(template),
  }),
});

export const sendTemplatesFilterWasPressedEventFx = attach({
  effect: logEventFx,
  mapParams: (filter: string | null) => ({
    name: events.TEMPLATES_FILTER_WAS_PRESSED_EVENT_NAME,
    ...getTemplateFilterEventPayload(filter),
  }),
});

export const sendTemplateDetailWasClosedEventFx = attach({
  effect: logEventFx,
  mapParams: (template: ITemplate) => ({
    name: events.TEMPLATE_WAS_CLOSED_EVENT_NAME,
    ...getTemplateEventPayload(template),
  }),
});

export const sendTemplateVideoAuthorWasPressedEventFx = attach({
  effect: logEventFx,
  mapParams: (template: ITemplate) => ({
    name: events.TEMPLATE_VIDEO_AUTHOR_WAS_PRESSED_EVENT_NAME,
    ...getTemplateEventPayload(template),
  }),
});

export const sendDecisionWasStartedEventFx = attach({
  effect: logEventFx,
  mapParams: (template?: ITemplate) => ({
    name: events.TEMPLATE_WAS_STARTED_EVENT_NAME,
    ...getTemplateEventPayload(template),
  }),
});

export const sendNewOptionWasPressedEventFx = attach({
  effect: logEventFx,
  mapParams: (params: IEventBasePayload) => ({
    name: events.NEW_OPTION_WAS_PRESSED_EVENT_NAME,
    decisionStep: params.step,
    ...getTemplateEventPayload(params.template),
  }),
});

export const sendNewOptionWasAddedEventFx = attach({
  effect: logEventFx,
  mapParams: (params: IOptionEventPayload) => ({
    name: events.NEW_OPTION_WAS_ADDED_EVENT_NAME,
    decisionStep: params.step,
    optionName: params.option,
    ...getTemplateEventPayload(params.template),
  }),
});

export const sendOptionWasDeletedEventFx = attach({
  effect: logEventFx,
  mapParams: (params: IOptionEventPayload) => ({
    name: events.OPTION_WAS_DELETED_EVENT_NAME,
    decisionStep: params.step,
    optionName: params.option,
    ...getTemplateEventPayload(params.template),
  }),
});

export const sendOptionWasEditedEventFx = attach({
  effect: logEventFx,
  mapParams: (params: IOptionEventPayload) => ({
    name: events.OPTION_WAS_RENAMED_EVENT_NAME,
    decisionStep: params.step,
    optionName: params.option,
    ...getTemplateEventPayload(params.template),
  }),
});

export const sendNewCriterionWasPressedEventFx = attach({
  effect: logEventFx,
  mapParams: (params: IEventBasePayload) => ({
    name: events.NEW_CRITERION_WAS_PRESSED_EVENT_NAME,
    decisionStep: params.step,
    ...getTemplateEventPayload(params.template),
  }),
});

export const sendNewCriterionWasAddedEventFx = attach({
  effect: logEventFx,
  mapParams: (params: ICriterionEventPayload) => ({
    name: events.NEW_CRITERION_WAS_ADDED_EVENT_NAME,
    decisionStep: params.step,
    criterionName: params.criterion,
    ...getTemplateEventPayload(params.template),
  }),
});

export const sendCriterionWasDeletedEventFx = attach({
  effect: logEventFx,
  mapParams: (params: ICriterionEventPayload) => ({
    name: events.CRITERION_WAS_DELETED_EVENT_NAME,
    decisionStep: params.step,
    criterionName: params.criterion,
    ...getTemplateEventPayload(params.template),
  }),
});

export const sendCriterionWasEditedEventFx = attach({
  effect: logEventFx,
  mapParams: (params: ICriterionEventPayload) => ({
    name: events.CRITERION_WAS_RENAMED_EVENT_NAME,
    decisionStep: params.step,
    criterionName: params.criterion,
    ...getTemplateEventPayload(params.template),
  }),
});

export const sendCriterionWeightWasChangedEventFx = attach({
  effect: logEventFx,
  mapParams: (params: IEventBasePayload) => ({
    name: events.CRITERION_WEIGHT_WAS_CHANGED_EVENT_NAME,
    decisionStep: params.step,
    ...getTemplateEventPayload(params.template),
  }),
});

export const sendPreviousStepButtonWasPressedEventFx = attach({
  effect: logEventFx,
  mapParams: (params: IEventBasePayload) => ({
    name: events.PREVIOUS_STEP_BUTTON_WAS_PRESSED_EVENT_NAME,
    decisionStep: params.step,
    ...getTemplateEventPayload(params.template),
  }),
});

export const sendNextStepButtonWasPressedEventFx = attach({
  effect: logEventFx,
  mapParams: (params: IEventBasePayload) => ({
    name: events.NEXT_STEP_BUTTON_WAS_PRESSED_EVENT_NAME,
    decisionStep: params.step,
    ...getTemplateEventPayload(params.template),
  }),
});

export const sendDecisionWasMadeEventFx = attach({
  effect: logEventFx,
  mapParams: (template?: ITemplate) => ({
    name: events.DECISION_WAS_MADE_EVENT_NAME,
    ...getTemplateEventPayload(template),
  }),
});

export const sendDecisionWasAlmostViewedEventFx = attach({
  effect: logEventFx,
  mapParams: (template?: ITemplate) => ({
    name: events.DECISION_WAS_ALMOST_VIEWED_EVENT_NAME,
    ...getTemplateEventPayload(template),
  }),
});
