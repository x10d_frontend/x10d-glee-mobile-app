import firestore from '@react-native-firebase/firestore';
import storage from '@react-native-firebase/storage';
import { createEffect } from 'effector';
import { omit } from 'ramda';

import { ITemplate, ITemplateTag } from './types';
import { IDecision } from '@entities/decision';

interface IFirebaseTemplate extends Omit<ITemplate, 'video'> {
  image: string;
}

interface IFirebaseTemplateTag extends Omit<ITemplateTag, 'id'> {}

export const getTemplatesFx = createEffect(
  async (): Promise<Array<ITemplate>> => {
    const response = await firestore()
      .collection<IFirebaseTemplate>('templates')
      .get();

    return Promise.all(
      response.docs.map(async doc => {
        const docData = doc.data();
        const imageRef = storage().ref(docData.image);
        const imageMetadata = await imageRef.getMetadata();

        return {
          ...docData,
          video: {
            url: await imageRef.getDownloadURL(),
            authorName: imageMetadata.customMetadata?.authorName,
            authorLink: imageMetadata.customMetadata?.authorLink,
          },
          id: doc.id,
        };
      }),
    );
  },
);

export const getTemplateTagsFx = createEffect(
  async (): Promise<Array<ITemplateTag>> => {
    const response = await firestore()
      .collection<IFirebaseTemplateTag>('template-tags')
      .get();

    return response.docs.map(doc => ({
      ...doc.data(),
      id: doc.id,
    }));
  },
);

export const saveDecisionFx = createEffect(async (payload: IDecision) => {
  await firestore()
    .collection('decisions')
    .add({
      ...omit(
        [
          'template',
          'criteriaDescription',
          'optionsDescription',
          'optionsTitle',
          'resultDescription',
        ],
        payload,
      ),
      templateId: payload.template?.id ?? null,
    });
});

export type { ITemplate, ITemplateTag };
