import { ITranslatableEntity } from '@shared/lib/i18n';

export interface ITemplate {
  id: string;

  title: ITranslatableEntity;
  description: ITranslatableEntity;
  descriptionShort: ITranslatableEntity;
  optionsTitle: ITranslatableEntity;
  optionsDescription: ITranslatableEntity;
  criteriaDescription: ITranslatableEntity;
  resultDescription: ITranslatableEntity;
  criteria: Array<{
    title: ITranslatableEntity;
  }>;

  featured: boolean;

  tags: Array<string>;

  video: {
    url: string;
    authorName?: string;
    authorLink?: string;
  };
}

export interface ITemplateTag {
  id: string;
  title: ITranslatableEntity;
}
