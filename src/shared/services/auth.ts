import auth, { FirebaseAuthTypes } from '@react-native-firebase/auth';
import { createEffect } from 'effector';

export const initializeFx = createEffect(
  () =>
    new Promise<FirebaseAuthTypes.User | null>(resolve => {
      auth().onAuthStateChanged(user => {
        resolve(user);
      });
    }),
);

export const signInAnonymouslyFx = createEffect(async () => {
  const response = await auth().signInAnonymously();

  return response.user;
});
