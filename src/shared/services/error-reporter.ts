import crashlytics from '@react-native-firebase/crashlytics';
import { FirebaseAuthTypes } from '@react-native-firebase/auth';
import { createEffect } from 'effector';

export const initializeFx = createEffect(
  async (viewer: FirebaseAuthTypes.User) => {
    await crashlytics().setUserId(viewer.uid);
  },
);

interface IReportErrorPayload {
  error: Error;
  errorInfo?: Record<string, string>;
  name?: string;
}
export const reportErrorFx = createEffect(
  async ({ error, errorInfo, name }: IReportErrorPayload) => {
    if (errorInfo !== undefined) {
      await crashlytics().setAttributes(errorInfo);
    }

    crashlytics().recordError(error, name);
  },
);
