import { FirebaseAuthTypes } from '@react-native-firebase/auth';
import { restore } from 'effector';
import { nthArg } from 'ramda';

import { signInAnonymouslyFx, initializeFx } from '@shared/services/auth';

export const $viewer = restore<FirebaseAuthTypes.User | null>(
  initializeFx.doneData,
  null,
).on(signInAnonymouslyFx.doneData, nthArg(1));

export const $viewerHasNotSignedIn = $viewer.map<boolean>(
  viewer => viewer === null,
);
