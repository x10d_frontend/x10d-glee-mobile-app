import { StyleSheet } from 'react-native';

import { color, spacing } from '@shared/ui';
import { deviceWidth } from '@shared/lib/dimensions';

export const TEMPLATE_ITEM_HEIGHT = 123;

export const styles = StyleSheet.create({
  template: {
    backgroundColor: color.palette.blackcurrant,
    borderRadius: 20,
    flexDirection: 'row',
    height: TEMPLATE_ITEM_HEIGHT,
    justifyContent: 'space-between',
    marginTop: spacing[4],
    overflow: 'hidden',
    paddingHorizontal: spacing[4],
    paddingTop: spacing[3],
  },

  template__image: {
    height: '100%',
    width: deviceWidth * 0.26,
  },

  template__tag: {
    fontSize: 15,
    fontWeight: '400',
    lineHeight: 18,
  },

  template__textContainer: {
    flex: 1,
    justifyContent: 'space-between',
    paddingBottom: spacing[4],
    paddingTop: spacing[2],
  },

  template__title: {
    fontSize: 19,
    fontWeight: '900',
    lineHeight: 23,
    maxWidth: deviceWidth * 0.41,
  },
});
