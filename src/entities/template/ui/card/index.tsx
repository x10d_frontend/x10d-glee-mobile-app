import React, { useCallback, useMemo, useRef } from 'react';
import { Pressable, StyleProp, View, ViewStyle } from 'react-native';
import { useStore } from 'effector-react';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import VisibilitySensor from '@svanboxel/visibility-sensor-react-native';
import ContentLoader, { Rect } from 'react-content-loader/native';
import debounce from 'lodash.debounce';

import { ITemplate } from '@shared/services/repository';
import { color, RemoteVideo, Text } from '@shared/ui';
import { deviceWidth } from '@shared/lib/dimensions';
import { translateEntity } from '@shared/lib/i18n';

import {
  $templateTagsMap,
  templateCardPressed,
  templateCardShown,
} from '../../model';

import { styles, TEMPLATE_ITEM_HEIGHT } from './styles';

interface ITemplateCardProps {
  item: ITemplate;
  style: StyleProp<ViewStyle>;
}

export const TemplateCard = function TemplateCard(props: ITemplateCardProps) {
  const analyticsEventWasCalled = useRef(false);
  const navigation = useNavigation();
  const templateTagsMap = useStore($templateTagsMap);

  const handleCardPress = () => {
    templateCardPressed(props.item);

    // @ts-expect-error
    navigation.navigate('templatePreview', {
      templateId: props.item.id,
    });
  };
  const handleCardVisibility = () => {
    if (!analyticsEventWasCalled.current && navigation.isFocused()) {
      templateCardShown(props.item);

      analyticsEventWasCalled.current = true;
    }
  };

  const debounceCardVisibilityHandler = useMemo(
    () => debounce(handleCardVisibility, 2000),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [props.item],
  );

  const initializeVisibilityChanging = (isVisible: boolean) => {
    if (isVisible) {
      debounceCardVisibilityHandler();

      return;
    }

    debounceCardVisibilityHandler.cancel();
  };

  useFocusEffect(
    useCallback(
      () => () => {
        analyticsEventWasCalled.current = false;

        debounceCardVisibilityHandler.cancel();
      },
      [debounceCardVisibilityHandler],
    ),
  );

  return (
    <VisibilitySensor onChange={initializeVisibilityChanging}>
      <Pressable
        onPress={handleCardPress}
        style={[styles.template, props.style]}
      >
        <View style={styles.template__textContainer}>
          <Text
            style={styles.template__title}
            preset="header"
            text={translateEntity(props.item.title)}
          />

          {props.item.tags.map(tag => (
            <Text
              key={tag}
              style={styles.template__tag}
              preset="subHeader"
              text={translateEntity(templateTagsMap[tag].title)}
            />
          ))}
        </View>

        <RemoteVideo
          style={styles.template__image}
          height={TEMPLATE_ITEM_HEIGHT}
          width={deviceWidth * 0.26}
          source={{ uri: props.item.video.url }}
        />
      </Pressable>
    </VisibilitySensor>
  );
};

export const TemplateCardSkeleton = function TemplateCardSkeleton(props: {
  style?: StyleProp<ViewStyle>;
}) {
  return (
    <View style={[styles.template, props.style]}>
      <ContentLoader
        backgroundColor={color.palette.mulledWine}
        foregroundColor={color.palette.mulledWineDarken}
      >
        <Rect width={132} height={20} rx={5} ry={5} y={6} />
        <Rect width={92} height={20} rx={5} ry={5} y={30} />
        <Rect width={79} height={15} rx={5} ry={5} y={74} />
        <Rect width={106} height={112} x={187} />
      </ContentLoader>
    </View>
  );
};
