import {
  combine,
  forward,
  restore,
  createEvent,
  createStore,
  createEffect,
} from 'effector';
import { nthArg } from 'ramda';

import { transformArrayToMap } from '@shared/lib';
import {
  getTemplatesFx,
  getTemplateTagsFx,
  ITemplate,
  ITemplateTag,
} from '@shared/services/repository';
import {
  sendTemplateCardWasPressedEventFx,
  sendTemplateCardWasShownEventFx,
  sendTemplateDetailWasClosedEventFx,
  sendTemplateVideoAuthorWasPressedEventFx,
} from '@shared/services/analytics';

import { getTemplatesWithCache, getTemplateTagsWithCache } from './lib';

interface ITemplateFilters {
  tag: string | null;
}

export const fetchTemplates = createEvent();
export const setFilters = createEvent<Partial<ITemplateFilters>>();
export const templateCardPressed = createEvent<ITemplate>();
export const templateCardShown = createEvent<ITemplate>();
export const templateDetailWasClosed = createEvent<ITemplate>();
export const templateVideoAuthorPressed = createEvent<ITemplate>();

const fetchTemplatesFx = createEffect<void, Array<ITemplate>, Error>(
  getTemplatesWithCache,
);

const fetchTemplateTagsFx = createEffect<void, Array<ITemplateTag>, Error>(
  getTemplateTagsWithCache,
);

export const $isLoading = combine(
  [fetchTemplatesFx.pending, fetchTemplateTagsFx.pending],
  ([isTemplatesLoading, isTagsLoading]) => isTemplatesLoading || isTagsLoading,
);

const $filters = createStore<ITemplateFilters>({
  tag: null,
}).on(setFilters, (state, payload) => ({
  ...state,
  ...payload,
}));
export const $selectedFilterTag = $filters.map(filters => filters.tag);

export const $templates = restore(getTemplatesFx.doneData, []).on(
  fetchTemplatesFx.doneData,
  nthArg(1),
);
export const $templatesMap = $templates.map(transformArrayToMap);
const $notFeaturedTemplates = $templates.map(templates =>
  templates.filter(template => !template.featured),
);
export const $filteredTemplates = combine(
  [$notFeaturedTemplates, $filters],
  ([notFeaturedTemplates, filters]) =>
    filters.tag === null
      ? notFeaturedTemplates
      : notFeaturedTemplates.filter(template =>
          template.tags.some(tag => tag === filters.tag),
        ),
);

export const $templateTags = restore(getTemplateTagsFx.doneData, []).on(
  fetchTemplateTagsFx.doneData,
  nthArg(1),
);
export const $templateTagsMap = $templateTags.map(transformArrayToMap);

forward({
  from: fetchTemplates,
  to: [fetchTemplatesFx, fetchTemplateTagsFx],
});

forward({
  from: templateCardPressed,
  to: sendTemplateCardWasPressedEventFx,
});

forward({
  from: templateCardShown,
  to: sendTemplateCardWasShownEventFx,
});

forward({
  from: templateDetailWasClosed,
  to: sendTemplateDetailWasClosedEventFx,
});

forward({
  from: templateVideoAuthorPressed,
  to: sendTemplateVideoAuthorWasPressedEventFx,
});
