import * as storage from '@shared/services/storage';
import { getTemplatesFx, getTemplateTagsFx } from '@shared/services/repository';

const TEMPLATES_STORAGE_KEY = 'GLEE.TEMPLATES';
const TEMPLATE_TAGS_STORAGE_KEY = 'GLEE.TEMPLATES_TAGS';

export const getTemplatesWithCache = async () => {
  const cache = await storage.load(TEMPLATES_STORAGE_KEY);

  if (cache === null) {
    const templates = await getTemplatesFx();
    await storage.save(TEMPLATES_STORAGE_KEY, templates);

    return templates;
  }

  return cache;
};

export const getTemplateTagsWithCache = async () => {
  const cache = await storage.load(TEMPLATE_TAGS_STORAGE_KEY);

  if (cache === null) {
    const tags = await getTemplateTagsFx();
    await storage.save(TEMPLATE_TAGS_STORAGE_KEY, tags);

    return tags;
  }

  return cache;
};
