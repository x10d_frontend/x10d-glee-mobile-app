import { translate } from '@shared/lib/i18n';

import { IDecision } from './model/types';

export const defaultDecision: IDecision = {
  userId: 'anonymous',
  template: null,
  value: {},
  createdAt: new Date(),
  options: [],
  criteria: [],
  optionsTitle: translate('template.optionsTitle'),
  optionsDescription: translate('template.optionsDescription'),
  criteriaDescription: translate('template.criteriaDescription'),
  resultDescription: translate('decision.resultDescription'),
};

export const MAX_VALUE = 5;
export const MIN_VALUE = 0;
