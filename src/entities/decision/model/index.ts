import {
  attach,
  combine,
  createEvent,
  createStore,
  forward,
  sample,
  Store,
} from 'effector';
import { omit, mergeDeepRight } from 'ramda';

import { sortTuplesBySecondElement } from '@shared/lib';
import { translateEntity } from '@shared/lib/i18n';
import {
  sendDecisionWasStartedEventFx,
  sendPreviousStepButtonWasPressedEventFx,
  sendNextStepButtonWasPressedEventFx,
  sendNewOptionWasPressedEventFx,
  sendNewOptionWasAddedEventFx,
  sendOptionWasDeletedEventFx,
  sendOptionWasEditedEventFx,
  sendNewCriterionWasPressedEventFx,
  sendNewCriterionWasAddedEventFx,
  sendCriterionWasDeletedEventFx,
  sendCriterionWasEditedEventFx,
  sendCriterionWeightWasChangedEventFx,
} from '@shared/services/analytics';
import { saveDecisionFx as saveDecisionToRepoFx } from '@shared/services/repository';

import { defaultDecision, MIN_VALUE, MAX_VALUE } from '../lib';
import type {
  ICreateDecisionEventParams,
  IDecision,
  ISetCriterionWeightEventParams,
} from './types';

export const createDecision = createEvent<ICreateDecisionEventParams>();
export const goToPreviousStep = createEvent<number>();
export const goToNextStep = createEvent<number>();
export const addOptionPressed = createEvent<number>();
export const addCriterionPressed = createEvent<number>();
export const addOption = createEvent<{ step: number; option: string }>();
export const addCriterion = createEvent<{ step: number; criterion: string }>();
export const deleteOption = createEvent<{ step: number; option: string }>();
export const deleteCriterion = createEvent<{
  step: number;
  criterion: string;
}>();
export const editOption = createEvent<{ step: number; option: string }>();
export const editCriterion = createEvent<{ step: number; criterion: string }>();
export const setCriterionWeight = createEvent<
  ISetCriterionWeightEventParams & { step: number }
>();

const decisionWasStarted = createDecision.map(({ template }) => template);

export const $currentDecision = createStore<IDecision | null>(null)
  .on(createDecision, (_, { viewer, template }) => ({
    userId: viewer?.uid ?? defaultDecision.userId,
    template: template ?? defaultDecision.template,
    options: defaultDecision.options,
    value: defaultDecision.value,
    createdAt: new Date(),
    criteria: template
      ? template.criteria.map(criterion => translateEntity(criterion.title))
      : defaultDecision.criteria,
    optionsTitle: template
      ? translateEntity(template.optionsTitle)
      : defaultDecision.optionsTitle,
    optionsDescription: template
      ? translateEntity(template.optionsDescription)
      : defaultDecision.optionsDescription,
    criteriaDescription: template
      ? translateEntity(template.criteriaDescription)
      : defaultDecision.criteriaDescription,
    resultDescription: template
      ? translateEntity(template.resultDescription)
      : defaultDecision.resultDescription,
  }))
  .on(addOption, (state, { option }) => {
    if (state !== null) {
      return {
        ...state,
        options: [option].concat(state.options),
      };
    }
  })
  .on([deleteOption, editOption], (state, { option }) => {
    if (state !== null) {
      return {
        ...state,
        value: omit([option], state.value),
        options: state.options.filter(item => item !== option),
      };
    }
  })
  .on(addCriterion, (state, { criterion }) => {
    if (state !== null) {
      return {
        ...state,
        criteria: [criterion].concat(state.criteria),
      };
    }
  })
  .on([deleteCriterion, editCriterion], (state, { criterion }) => {
    if (state !== null) {
      return {
        ...state,
        value: Object.keys(state.value).reduce((acc, option) => {
          acc[option] = Object.fromEntries(
            Object.entries(state.value[option]).filter(
              ([item]) => item !== criterion,
            ),
          );

          return acc;
        }, {} as IDecision['value']),
        criteria: state.criteria.filter(item => item !== criterion),
      };
    }
  })
  .on([setCriterionWeight], (state, { value, option, criterion }) => {
    if (value > MAX_VALUE || value < MIN_VALUE) {
      return;
    }

    if (state !== null) {
      return {
        ...state,
        value: mergeDeepRight(state.value, {
          [option]: {
            [criterion]: value,
          },
        }),
      };
    }
  });

const $decisionValue = $currentDecision.map(state =>
  state !== null ? state.value : defaultDecision.value,
);
const $defaultValue = $currentDecision.map(state =>
  state !== null
    ? state.options.reduce((acc, option) => {
        state.criteria.forEach(criterion => {
          acc[option] = {
            ...acc[option],
            [criterion]: 0,
          };
        });

        return acc;
      }, {} as IDecision['value'])
    : {},
);
const $isAddingOptionAllowed = $currentDecision.map<boolean>(decision =>
  decision !== null ? decision.options.length < 4 : false,
);
const $isOptionsCountValid = $currentDecision.map<boolean>(decision =>
  decision !== null ? decision.options.length > 1 : false,
);
const $isCriteriaCountValid = $currentDecision.map<boolean>(decision =>
  decision !== null ? decision.criteria.length > 1 : false,
);
const $wasValueChanged = $currentDecision.map<boolean>(decision =>
  decision !== null
    ? Object.values(decision.value)
        .map(option => Object.values(option))
        .flat()
        .some(value => value > 0)
    : false,
);
const $valuesMaxSum = $currentDecision.map<number>(decision =>
  decision !== null ? decision.criteria.length * MAX_VALUE : 0,
);

export const $finalValue = combine(
  [$decisionValue, $defaultValue],
  ([decisionValue, defaultValue]) =>
    mergeDeepRight(defaultValue, decisionValue),
);

/**
 * Сумма значений всех критериев параметра,
 * отсортированная по убыванию.
 */
export const $optionsTotalSum = combine(
  [$finalValue, $valuesMaxSum],
  ([finalValue, valuesMaxSum]) =>
    Object.entries(finalValue)
      .reduce<Array<[string, number]>>((acc, [option, criteriaValues]) => {
        const optionCriteriaSum: number = Object.values(criteriaValues).reduce(
          (sum, value) => sum + value,
          0,
        );
        const itemToPush: [string, number] = [
          option,
          Math.round((optionCriteriaSum / valuesMaxSum) * 100),
        ];

        acc.push(itemToPush);

        return acc;
      }, [])
      .sort(sortTuplesBySecondElement),
);

export const $currentDecisionStore = combine(
  [
    $currentDecision,
    $isAddingOptionAllowed,
    $isOptionsCountValid,
    $isCriteriaCountValid,
    $wasValueChanged,
  ],
  ([
    currentDecision,
    isAddingOptionAllowed,
    isOptionsCountValid,
    isCriteriaCountValid,
    wasValueChanged,
  ]) => ({
    currentDecision,
    isAddingOptionAllowed,
    isOptionsCountValid,
    isCriteriaCountValid,
    wasValueChanged,
  }),
);

export const saveDecisionFx = attach({
  effect: saveDecisionToRepoFx,
  source: $currentDecision as Store<IDecision>,
  mapParams: (_, decision) => decision,
});
export const $isDecisionSaving = saveDecisionFx.pending;

forward({
  from: decisionWasStarted,
  to: sendDecisionWasStartedEventFx,
});

sample({
  clock: goToPreviousStep,
  source: $currentDecision,
  fn: (decision, currentStep) => ({
    step: currentStep,
    template: decision?.template || undefined,
  }),
  target: sendPreviousStepButtonWasPressedEventFx,
});

sample({
  clock: goToNextStep,
  source: $currentDecision,
  fn: (decision, currentStep) => ({
    step: currentStep,
    template: decision?.template || undefined,
  }),
  target: sendNextStepButtonWasPressedEventFx,
});

sample({
  clock: addOptionPressed,
  source: $currentDecision,
  fn: (decision, currentStep) => ({
    step: currentStep,
    template: decision?.template || undefined,
  }),
  target: sendNewOptionWasPressedEventFx,
});

sample({
  clock: addOption,
  source: $currentDecision,
  fn: (decision, { step, option }) => ({
    step,
    option,
    template: decision?.template || undefined,
  }),
  target: sendNewOptionWasAddedEventFx,
});

sample({
  clock: deleteOption,
  source: $currentDecision,
  fn: (decision, { step, option }) => ({
    step,
    option,
    template: decision?.template || undefined,
  }),
  target: sendOptionWasDeletedEventFx,
});

sample({
  clock: editOption,
  source: $currentDecision,
  fn: (decision, { step, option }) => ({
    step,
    option,
    template: decision?.template || undefined,
  }),
  target: sendOptionWasEditedEventFx,
});

sample({
  clock: addCriterionPressed,
  source: $currentDecision,
  fn: (decision, currentStep) => ({
    step: currentStep,
    template: decision?.template || undefined,
  }),
  target: sendNewCriterionWasPressedEventFx,
});

sample({
  clock: addCriterion,
  source: $currentDecision,
  fn: (decision, { step, criterion }) => ({
    step,
    criterion,
    template: decision?.template || undefined,
  }),
  target: sendNewCriterionWasAddedEventFx,
});

sample({
  clock: deleteCriterion,
  source: $currentDecision,
  fn: (decision, { step, criterion }) => ({
    step,
    criterion,
    template: decision?.template || undefined,
  }),
  target: sendCriterionWasDeletedEventFx,
});

sample({
  clock: editCriterion,
  source: $currentDecision,
  fn: (decision, { step, criterion }) => ({
    step,
    criterion,
    template: decision?.template || undefined,
  }),
  target: sendCriterionWasEditedEventFx,
});

sample({
  clock: setCriterionWeight,
  source: $currentDecision,
  fn: (decision, { step }) => ({
    step,
    template: decision?.template || undefined,
  }),
  target: sendCriterionWeightWasChangedEventFx,
});
