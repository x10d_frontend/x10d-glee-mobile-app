import { FirebaseAuthTypes } from '@react-native-firebase/auth';

import { ITemplate } from '@shared/services/repository';

export interface IDecision {
  userId: string;
  template: ITemplate | null;
  createdAt: Date;
  options: Array<string>;
  criteria: Array<string>;

  value: Record<string, Record<string, number>>;

  optionsTitle: string;
  optionsDescription: string;
  criteriaDescription: string;
  resultDescription: string;
}

export interface ICreateDecisionEventParams {
  template?: ITemplate;
  viewer: FirebaseAuthTypes.User | null;
}

export interface ISetCriterionWeightEventParams {
  value: number;
  option: string;
  criterion: string;
}
