import { StyleSheet } from 'react-native';

import { deviceHeight } from '@shared/lib/dimensions';

import { mainButtonHeight, mainSidePadding, spacing } from '@shared/ui/theme';

export const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
  },

  title: {
    marginTop: 0.4 * deviceHeight,
    fontSize: 19,
    fontWeight: '900',
  },

  description: {
    marginTop: spacing[5],
  },

  button: {
    alignSelf: 'stretch',
    borderRadius: mainButtonHeight / 2,
    height: spacing[7],
    marginTop: spacing[8],
    marginHorizontal: mainSidePadding,
  },
});
