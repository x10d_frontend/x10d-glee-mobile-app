import React from 'react';
import { ImageBackground, StatusBar } from 'react-native';

import { Button, Text } from '@shared/ui';

import { styles } from './styles';

const bg = require('./bg.png');

export interface ErrorComponentProps {
  onReset(): void;
}

export const ErrorComponent = (props: ErrorComponentProps) => {
  return (
    <ImageBackground source={bg} style={styles.container}>
      <StatusBar barStyle="light-content" />

      <Text tx="errorScreen.title" preset="body" style={styles.title} />

      <Text
        tx="errorScreen.description"
        preset="default"
        style={styles.description}
      />

      <Button
        style={styles.button}
        tx="errorScreen.reset"
        onPress={props.onReset}
        preset="dark"
      />
    </ImageBackground>
  );
};
