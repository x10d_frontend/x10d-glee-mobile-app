import React, { Component, ErrorInfo, ReactNode } from 'react';

import { reportErrorFx } from '@shared/services/error-reporter';

import { ErrorComponent } from './error-component';

interface Props {
  children: ReactNode;
  catchErrors: 'always' | 'dev' | 'prod' | 'never';
}

interface State {
  error: Error | null;
  errorInfo: ErrorInfo | null;
}

/**
 * Этот компонент обрабатывает JS ошибки, когда они возникают в приложении.
 * Здесь используется классовый компонент, потому что согласно документации
 * только классовые компоненты могут отлавливать ошибки.
 *
 * Смотри:
 *
 * @link: https://reactjs.org/docs/error-boundaries.html
 */
export class ErrorBoundary extends Component<Props, State> {
  state = { error: null, errorInfo: null };

  async componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    this.setState({
      error,
      errorInfo,
    });

    await reportErrorFx({
      error,
      // @ts-expect-error
      errorInfo,
      name: 'CaughtByErrorBoundary',
    });
  }

  resetError = async () => {
    this.setState({ error: null, errorInfo: null });
  };

  shouldComponentUpdate(
    nextProps: Readonly<any>,
    nextState: Readonly<any>,
  ): boolean {
    return nextState.error !== nextProps.error;
  }

  isEnabled(): boolean {
    return (
      this.props.catchErrors === 'always' ||
      (this.props.catchErrors === 'dev' && __DEV__) ||
      (this.props.catchErrors === 'prod' && !__DEV__)
    );
  }

  render() {
    return this.isEnabled() && this.state.error ? (
      <ErrorComponent onReset={this.resetError} />
    ) : (
      this.props.children
    );
  }
}
