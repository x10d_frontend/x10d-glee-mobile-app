import { combine, forward } from 'effector';

import { translateEntity, translate } from '@shared/lib/i18n';
import {
  setFilters,
  $templateTags,
  $selectedFilterTag,
} from '@entities/template';

import { sendTemplatesFilterWasPressedEventFx } from '@shared/services/analytics';

export const setTemplateTag = setFilters.prepend<string | null>(tag => ({
  tag,
}));

interface ITemplateTagFilter {
  id: string | null;
  title: string;
}

const $templateTagsFilters = $templateTags.map<Array<ITemplateTagFilter>>(
  tags =>
    [
      {
        id: null as string | null,
        title: translate('all'),
      },
    ].concat(
      tags.map(tag => ({
        id: tag.id,
        title: translateEntity(tag.title),
      })),
    ),
);

export const $templateTagsFiltersStore = combine(
  [$templateTagsFilters, $selectedFilterTag],
  ([templateTagsFilters, selectedFilterTag]) => ({
    templateTagsFilters,
    selectedFilterTag,
  }),
);

forward({
  from: setTemplateTag,
  to: sendTemplatesFilterWasPressedEventFx,
});
