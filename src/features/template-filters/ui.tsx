import * as React from 'react';
import {
  Pressable,
  ScrollView,
  StyleProp,
  StyleSheet,
  ViewStyle,
} from 'react-native';
import { useStore } from 'effector-react';

import { Text, mainSidePadding, color, spacing } from '@shared/ui';

import { $templateTagsFiltersStore, setTemplateTag } from './model';

interface TemplatesFiltersProps {
  style?: StyleProp<ViewStyle>;
}

const styles = StyleSheet.create({
  container: {
    marginHorizontal: -mainSidePadding,
  },

  tag: {
    borderColor: color.palette.blackcurrant,
    borderRadius: spacing[2],
    borderWidth: 1,
    height: spacing[6],
    justifyContent: 'center',
    marginRight: spacing[2],
    paddingHorizontal: spacing[4],
  },

  tagText_selected: {
    color: color.palette.heliotrope,
  },

  tag_selected: {
    backgroundColor: color.palette.blackcurrant,
  },
});

export const TemplatesFilters = function TemplatesFilters(
  props: TemplatesFiltersProps,
) {
  const { templateTagsFilters, selectedFilterTag } = useStore(
    $templateTagsFiltersStore,
  );

  return (
    <ScrollView
      style={[props.style, styles.container]}
      horizontal
      bounces={false}
      showsHorizontalScrollIndicator={false}
    >
      {templateTagsFilters.map((tag, index, array) => (
        <Pressable
          key={tag.id}
          onPress={() => setTemplateTag(tag.id)}
          style={[
            styles.tag,
            index === 0 ? { marginLeft: mainSidePadding } : null,
            index === array.length - 1
              ? { marginRight: mainSidePadding }
              : null,
            selectedFilterTag === tag.id ? styles.tag_selected : null,
          ]}
        >
          <Text
            style={
              selectedFilterTag === tag.id ? styles.tagText_selected : null
            }
            text={tag.title}
          />
        </Pressable>
      ))}
    </ScrollView>
  );
};
