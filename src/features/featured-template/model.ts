import { combine } from 'effector';

import { $templates } from '@entities/template';

const $featuredTemplates = $templates.map(templates =>
  templates.filter(template => template.featured),
);

export const $featuredTemplate = combine(
  [$templates, $featuredTemplates],
  ([templates, featuredTemplates]) =>
    templates.length !== 0 ? featuredTemplates[0] ?? templates[0] : null,
);
