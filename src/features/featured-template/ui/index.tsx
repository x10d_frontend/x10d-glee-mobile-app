import React, { useCallback, useRef } from 'react';
import { Pressable, View } from 'react-native';
import { useStore } from 'effector-react';
import { useFocusEffect, useNavigation } from '@react-navigation/native';
import VisibilitySensor from '@svanboxel/visibility-sensor-react-native';

import { templateCardPressed, templateCardShown } from '@entities/template';

import { Icon, Text } from '@shared/ui';
import { translateEntity } from '@shared/lib/i18n';

import { $featuredTemplate } from '../model';
import { styles } from './styles';

export const FeaturedTemplate = function FeaturedTemplate() {
  const analyticsEventWasCalled = useRef(false);

  const template = useStore($featuredTemplate);
  const navigation = useNavigation();

  const handlePress = () => {
    if (template !== null) {
      templateCardPressed(template);

      // @ts-expect-error
      navigation.navigate('templatePreview', {
        templateId: template.id,
      });
    }
  };

  const handleVisibility = (isVisible: boolean) => {
    if (
      isVisible &&
      !analyticsEventWasCalled.current &&
      navigation.isFocused() &&
      template !== null
    ) {
      templateCardShown(template);

      analyticsEventWasCalled.current = true;
    }
  };

  useFocusEffect(
    useCallback(
      () => () => {
        analyticsEventWasCalled.current = false;
      },
      [],
    ),
  );

  if (!template) {
    return null;
  }

  return (
    <VisibilitySensor onChange={handleVisibility}>
      <Pressable style={styles.container} onPress={handlePress}>
        <View>
          <View style={styles.badge}>
            <Text
              tx="template.featuredBadge"
              style={styles.badgeText}
              preset="subHeader"
            />
          </View>

          <Text
            preset="header"
            text={translateEntity(template.title)}
            style={styles.title}
          />

          <Text
            preset="subHeader"
            text={translateEntity(template.descriptionShort)}
            style={styles.subTitle}
          />
        </View>

        <Icon icon="chevron" style={styles.icon} />
      </Pressable>
    </VisibilitySensor>
  );
};
