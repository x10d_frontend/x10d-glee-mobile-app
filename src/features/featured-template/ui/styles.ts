import { StyleSheet } from 'react-native';

import { color, spacing } from '@shared/ui';
import { deviceWidth } from '@shared/lib/dimensions';

export const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },

  icon: { transform: [{ rotate: '180deg' }] },

  badge: {
    height: 22,
    alignSelf: 'flex-start',
    backgroundColor: color.palette.blueRibbon,
    paddingHorizontal: spacing[2],
    borderRadius: spacing[2],
  },

  badgeText: {
    color: color.text,
    fontWeight: '800',
  },

  subTitle: {
    color: color.text,
    marginTop: spacing[4],
    maxWidth: deviceWidth * 0.6,
  },

  title: {
    marginTop: spacing[3],
    maxWidth: deviceWidth * 0.7,
  },
});
