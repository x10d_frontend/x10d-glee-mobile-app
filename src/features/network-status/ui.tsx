import React from 'react';
import { ImageBackground, StyleSheet } from 'react-native';

import { Text, color } from '@shared/ui';
import { deviceHeight, deviceWidth } from '@shared/lib/dimensions';

import { useNetworkStatus } from './lib';

const bg = require('./net-info-bg.png');

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    width: deviceWidth,
    height: deviceHeight * 0.14,
    backgroundColor: color.transparent,
    zIndex: 10,
  },
});

export const OfflineBadge = function OfflineBadge() {
  const { isOnline } = useNetworkStatus();

  if (isOnline) {
    return null;
  }

  return (
    <ImageBackground
      source={bg}
      style={[styles.container, StyleSheet.absoluteFill]}
    >
      <Text tx={'networkUnavailableMessage'} preset="default" />
    </ImageBackground>
  );
};
