import { useState, useEffect } from 'react';
import { addEventListener } from '@react-native-community/netinfo';

export const useNetworkStatus = () => {
  const [isOnline, setIsOnline] = useState<boolean>(true);

  useEffect(
    () =>
      addEventListener(networkState => {
        setIsOnline(!!networkState.isConnected);
      }),
    [],
  );

  return { isOnline };
};
