import React, { useEffect } from 'react';
import { useStore } from 'effector-react';

import '@shared/config/ignore-warnings';
import '@shared/lib/i18n';

import { OfflineBadge } from '@features/network-status';
import { ErrorBoundary } from '@features/error-boundary';

import { AppNavigator } from './navigation';

import { $isInitialized, initializeApp, navigationReady } from './model';
import { withProviders } from './providers';

const App = () => {
  const isInitialized = useStore($isInitialized);

  useEffect(() => {
    initializeApp();
  }, []);

  if (!isInitialized) {
    return null;
  }

  return (
    <>
      <OfflineBadge />

      <ErrorBoundary catchErrors="always">
        <AppNavigator onReady={navigationReady} />
      </ErrorBoundary>
    </>
  );
};

export default withProviders(App);
