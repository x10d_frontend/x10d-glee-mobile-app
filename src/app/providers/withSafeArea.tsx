import React from 'react';
import {
  SafeAreaProvider,
  initialWindowMetrics,
} from 'react-native-safe-area-context';

export const withSafeArea = (Component: React.FC) => () =>
  (
    <SafeAreaProvider initialMetrics={initialWindowMetrics}>
      <Component />
    </SafeAreaProvider>
  );
