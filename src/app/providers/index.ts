import { pipe } from 'ramda';

import { withSafeArea } from './withSafeArea';

export const withProviders = pipe(withSafeArea);
