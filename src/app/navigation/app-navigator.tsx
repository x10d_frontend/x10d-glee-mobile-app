import React from 'react';
import { Pressable, useColorScheme } from 'react-native';
import {
  NavigationContainer,
  DefaultTheme,
  DarkTheme,
} from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import { TemplatesScreen } from '@pages/templates';
import { TemplatePreviewScreen } from '@pages/template-preview';

import { Icon, spacing } from '@shared/ui';

import { DecisionStack } from './decision-navigator';
import { navigationRef, useBackButtonHandler, canExit } from './lib';

export type NavigatorParamList = {
  templates: undefined;

  templatePreview: {
    templateId: string;
  };

  decisionStack: undefined;
};

const Stack = createNativeStackNavigator<NavigatorParamList>();

const AppStack = () => {
  return (
    <Stack.Navigator initialRouteName="templates">
      <Stack.Screen
        name="templates"
        component={TemplatesScreen}
        options={{ headerShown: false }}
      />

      <Stack.Screen
        name="templatePreview"
        component={TemplatePreviewScreen}
        options={({ navigation }) => ({
          headerTransparent: true,
          headerTitle: '',
          headerLeft: () => (
            <Pressable
              onPress={navigation.goBack}
              style={{ marginLeft: spacing[2] }}
              hitSlop={{
                top: 20,
                right: 20,
                bottom: 20,
                left: 20,
              }}
            >
              <Icon icon="chevron" />
            </Pressable>
          ),
        })}
      />

      <Stack.Screen
        name="decisionStack"
        component={DecisionStack}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};

interface NavigationProps
  extends Partial<React.ComponentProps<typeof NavigationContainer>> {}

export const AppNavigator = (props: NavigationProps) => {
  const colorScheme = useColorScheme();

  useBackButtonHandler(canExit);

  return (
    <NavigationContainer
      ref={navigationRef}
      theme={colorScheme === 'dark' ? DarkTheme : DefaultTheme}
      {...props}
    >
      <AppStack />
    </NavigationContainer>
  );
};

AppNavigator.displayName = 'AppNavigator';
