import React from 'react';
import { Pressable } from 'react-native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import { TemplateOptionsScreen } from '@pages/template-options';
import { TemplateCriteriaScreen } from '@pages/template-criteria';
import { TemplateCriteriaValuesScreen } from '@pages/template-criteria-values';
import { DecisionScreen } from '@pages/decision';

import { goToPreviousStep } from '@entities/decision';

import { Icon, spacing } from '@shared/ui';

export type NavigatorParamList = {
  decisionOptions: undefined;
  decisionCriteria: undefined;
  decisionCriteriaValues: undefined;
  decisionResult: undefined;
};

const Stack = createNativeStackNavigator<NavigatorParamList>();

export const DecisionStack = () => {
  return (
    <Stack.Navigator
      screenOptions={({ navigation }) => ({
        headerTransparent: true,
        headerTitle: '',
        headerLeft: () => (
          <Pressable
            onPress={() => {
              goToPreviousStep(navigation.getState().index + 1);

              navigation.goBack();
            }}
            style={{ marginLeft: spacing[2] }}
            hitSlop={{
              top: 20,
              right: 20,
              bottom: 20,
              left: 20,
            }}
          >
            <Icon icon="chevron" />
          </Pressable>
        ),
      })}
      initialRouteName="decisionOptions"
    >
      <Stack.Screen name="decisionOptions" component={TemplateOptionsScreen} />
      <Stack.Screen
        name="decisionCriteria"
        component={TemplateCriteriaScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="decisionCriteriaValues"
        component={TemplateCriteriaValuesScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="decisionResult"
        component={DecisionScreen}
        options={{ headerShown: false, gestureEnabled: false }}
      />
    </Stack.Navigator>
  );
};
