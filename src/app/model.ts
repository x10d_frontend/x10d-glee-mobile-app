import {
  createStore,
  createEffect,
  createEvent,
  sample,
  Store,
} from 'effector';
import { combineEvents } from 'patronum';
import { AppState } from 'react-native';
import RNBootSplash from 'react-native-bootsplash';

import { $viewerHasNotSignedIn, $viewer } from '@entities/viewer';

import {
  initializeFx as initAuthFx,
  signInAnonymouslyFx,
} from '@shared/services/auth';
import { initializeFx as initAnalyticsFx } from '@shared/services/analytics';
import { initializeFx as initErrorReporterFx } from '@shared/services/error-reporter';

import * as storage from '@shared/services/storage';
import { FirebaseAuthTypes } from '@react-native-firebase/auth';

export const initializeApp = createEvent();
export const navigationReady = createEvent();

const clearStorageFx = createEffect(async () => {
  if (AppState.currentState === 'unknown') {
    await storage.clear();
  }
});

const hideSplashFx = createEffect(async () => {
  await RNBootSplash.hide({ fade: true });
});

const handleAuthFx = createEffect(async (viewerHasNotSignedIn: boolean) => {
  if (viewerHasNotSignedIn) {
    await signInAnonymouslyFx();
  }
});

export const $isInitialized = createStore<boolean>(false);

sample({
  clock: initializeApp,
  target: initAuthFx,
});

sample({
  clock: initAuthFx.done,
  source: $viewerHasNotSignedIn,
  target: handleAuthFx,
});

sample({
  clock: handleAuthFx.done,
  source: $viewer as Store<FirebaseAuthTypes.User>,
  target: [initErrorReporterFx, initAnalyticsFx, clearStorageFx],
});

sample({
  clock: combineEvents({
    events: [
      initErrorReporterFx.done,
      initAnalyticsFx.done,
      clearStorageFx.done,
    ],
  }),
  source: $viewer,
  fn: () => true,
  target: $isInitialized,
});

sample({
  clock: navigationReady,
  target: hideSplashFx,
});
