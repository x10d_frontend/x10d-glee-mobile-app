import { StyleSheet } from 'react-native';

import {
  mainSidePadding,
  screenFirstElementOffset,
  spacing,
  mainButtonHeight,
} from '@shared/ui';

export const styles = StyleSheet.create({
  screenButton__icon_back: {
    justifyContent: 'center',
  },

  screenButton__icon_next: {
    transform: [{ rotate: '180deg' }],
  },

  screen__button_back: {
    marginRight: spacing[3],
  },

  screen__button_next: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  screen__buttons: {
    flexDirection: 'row',
    left: mainSidePadding,
    position: 'absolute',
    right: mainSidePadding,
  },

  screen__description: {
    flex: 1,
    marginTop: spacing[2],
  },

  screen__list: {
    marginBottom: mainButtonHeight + spacing[5],
    marginTop: spacing[6],
  },

  screen__title: {
    marginTop: screenFirstElementOffset,
  },
});
