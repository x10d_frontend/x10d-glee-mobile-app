import React from 'react';
import { View } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { useNavigation } from '@react-navigation/native';
import { useStore } from 'effector-react';

import {
  $currentDecisionStore,
  goToNextStep,
  goToPreviousStep,
  addCriterionPressed,
  addCriterion,
  deleteCriterion,
  editCriterion,
} from '@entities/decision';

import {
  ScreenWithVideoHeader,
  Button,
  EditableList,
  GradientBackground,
  Icon,
  Text,
  color,
} from '@shared/ui';
import { deviceHeight } from '@shared/lib/dimensions';

import { styles } from './styles';

const templateDetails = require('@shared/assets/template-details.mp4');

export const TemplateCriteriaScreen = function TemplateCriteriaScreen() {
  const { currentDecision: decision, isCriteriaCountValid } = useStore(
    $currentDecisionStore,
  );

  const navigation = useNavigation();
  const { bottom: bottomInset } = useSafeAreaInsets();

  const isNextButtonDisabled = !isCriteriaCountValid;
  const currentStep = navigation.getState().index + 1;

  const handleBackButtonPress = () => {
    goToPreviousStep(currentStep);

    navigation.goBack();
  };
  const handleNextButtonPress = () => {
    goToNextStep(currentStep);

    // @ts-expect-error
    navigation.navigate('decisionCriteriaValues');
  };
  const handleAddButtonPress = () => {
    addCriterionPressed(currentStep);
  };
  const handleAddingCriterion = (criterion: string) => {
    addCriterion({
      step: currentStep,
      criterion,
    });
  };
  const handleDeletingCriterion = (criterion: string) => {
    deleteCriterion({
      step: currentStep,
      criterion,
    });
  };
  const handleEditingCriterion = (criterion: string) => {
    editCriterion({
      step: currentStep,
      criterion,
    });
  };

  if (!decision) {
    return null;
  }

  return (
    <>
      <ScreenWithVideoHeader
        videoSource={templateDetails}
        headerHeight={deviceHeight * 0.4}
      >
        <Text
          style={styles.screen__title}
          tx="template.criteriaTitle"
          preset="header"
        />

        <Text
          style={styles.screen__description}
          text={decision.criteriaDescription}
          preset="body_l"
        />

        <EditableList
          items={decision.criteria}
          onAddItem={handleAddingCriterion}
          onDeleteItem={handleDeletingCriterion}
          onEditItem={handleEditingCriterion}
          onAddButtonPress={handleAddButtonPress}
          isAddingItemsAllowed
          style={styles.screen__list}
        />
      </ScreenWithVideoHeader>

      <GradientBackground
        pointerEvents="none"
        locations={[0.85, 1]}
        colors={['#09011000', '#090110']}
      />

      <View style={[styles.screen__buttons, { bottom: bottomInset }]}>
        <Button
          style={styles.screen__button_back}
          onPress={handleBackButtonPress}
          preset="dark_rounded"
        >
          <Icon
            icon="chevron"
            containerStyle={styles.screenButton__icon_back}
          />
        </Button>

        <Button
          style={styles.screen__button_next}
          tx="buttons.next"
          disabled={isNextButtonDisabled}
          onPress={handleNextButtonPress}
          preset="pink"
        >
          <Icon
            icon="chevron"
            style={[
              styles.screenButton__icon_next,
              isNextButtonDisabled
                ? { tintColor: color.palette.mulledWine }
                : null,
            ]}
          />
        </Button>
      </View>
    </>
  );
};
