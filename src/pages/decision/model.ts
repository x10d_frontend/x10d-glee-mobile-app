import { createEvent, combine, sample } from 'effector';

import {
  $currentDecision,
  $finalValue,
  $optionsTotalSum,
} from '@entities/decision';

import { sortTuplesBySecondElement } from '@shared/lib';

import {
  sendDecisionWasAlmostViewedEventFx,
  sendDecisionWasMadeEventFx,
} from '@shared/services/analytics';

export const decisionWasMade = createEvent<void>();
export const decisionAlmostWhollyViewed = createEvent<void>();

/**
 * Значения всех критериев параметра.
 *
 * Например, если критерии это "Зарплата" и "Задачи",
 * параметры - "МТС" и "Сбербанк", то в результате
 * будет следующая структура:
 *
 * @example
 * [
 *   ['МТС', [5, 4]],
 *   ['Сбербанк', [4, 5]]
 * ]
 */
const $optionsAllCriteriaValues = combine(
  [$optionsTotalSum, $finalValue],
  ([optionsTotalSum, finalValue]) =>
    optionsTotalSum.map(([option]) => [
      option,
      Object.values(finalValue[option]),
    ]),
);

/**
 * Сгруппированные по критериям значения параметров,
 * отсортированные от большего к меньшему.
 *
 * Например, если критерии это "Зарплата" и "Задачи",
 * параметры - "МТС" и "Сбербанк", то в результате
 * будет следующая структура:
 *
 * @example
 * [
 *   ['Зарплата', [['МТС', 5], ['Сбербанк', 4]],
 *   ['Задачи', [['Сбербанк', 5], ['МТС', 4]]
 * ]
 */
const $groupedByCriteriaOptionsValues = combine(
  [$currentDecision, $finalValue],
  ([decision, finalValue]) =>
    decision !== null
      ? decision.criteria.map(criteria => [
          criteria,
          decision.options
            .map<[string, number]>(option => [
              option,
              finalValue[option][criteria],
            ])
            .sort(sortTuplesBySecondElement),
        ])
      : [],
);

export const $decisionResultStore = combine(
  [
    $currentDecision,
    $optionsTotalSum,
    $optionsAllCriteriaValues,
    $groupedByCriteriaOptionsValues,
  ],
  ([
    currentDecision,
    optionsTotalSum,
    optionsAllCriteriaValues,
    groupedByCriteriaOptionsValues,
  ]) => ({
    currentDecision,
    optionsTotalSum,
    bestResultName:
      optionsTotalSum.length > 0 ? optionsTotalSum[0][0] : undefined,
    bestResultValue:
      optionsTotalSum.length > 0 ? optionsTotalSum[0][1] : undefined,
    optionsAllCriteriaValues: optionsAllCriteriaValues as Array<
      [string, Array<number>]
    >,
    groupedByCriteriaOptionsValues: groupedByCriteriaOptionsValues as Array<
      [string, Array<[string, number]>]
    >,
  }),
);

sample({
  clock: decisionWasMade,
  source: $currentDecision,
  fn: decision => decision?.template ?? undefined,
  target: sendDecisionWasMadeEventFx,
});

sample({
  clock: decisionAlmostWhollyViewed,
  source: $currentDecision,
  fn: decision => decision?.template ?? undefined,
  target: sendDecisionWasAlmostViewedEventFx,
});
