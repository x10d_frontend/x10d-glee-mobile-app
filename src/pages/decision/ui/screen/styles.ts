import { StyleSheet } from 'react-native';

import {
  color,
  mainSidePadding,
  screenFirstElementOffset,
  spacing,
  mainButtonHeight,
} from '@shared/ui/theme';
import { deviceHeight, deviceWidth } from '@shared/lib/dimensions';

export const styles = StyleSheet.create({
  bestResult__name: {
    marginTop: spacing[1],
    maxWidth: deviceWidth * 0.7,
  },
  bestResult__value: {
    marginTop: spacing[5],
  },
  bestResult_title: {
    marginTop: screenFirstElementOffset,
  },

  container: {
    paddingBottom: mainSidePadding,
  },

  content: {
    marginBottom: spacing[8],
    marginHorizontal: mainSidePadding,
  },

  content__bg: {
    marginHorizontal: -mainSidePadding,
    marginTop: spacing[7],
  },
  content__button: {
    alignSelf: 'center',
    borderRadius: mainButtonHeight / 2,
    height: spacing[7],
    marginTop: spacing[8],
    paddingHorizontal: spacing[7],
    width: deviceWidth * 0.6,
  },
  content__image: {
    height: deviceHeight,
  },
});

export const INDICATOR_COLORS: Array<string> = [
  color.palette.heliotrope,
  color.palette.brightTurquoise,
  color.palette.mediumPurple,
  color.palette.radicalRed,
];
