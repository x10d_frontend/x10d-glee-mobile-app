import React, { useEffect, useRef } from 'react';
import { ImageBackground, View } from 'react-native';
import { useStore } from 'effector-react';
import { useNavigation } from '@react-navigation/native';

import { saveDecisionFx, $isDecisionSaving } from '@entities/decision';

import { Text, ScreenWithVideoHeader } from '@shared/ui';
import { deviceHeight } from '@shared/lib/dimensions';

import {
  $decisionResultStore,
  decisionWasMade,
  decisionAlmostWhollyViewed,
} from '../../model';

import { DecisionScreenChart } from '../chart';
import { DecisionScreenTotal } from '../total';
import { DecisionScreenResultDescription } from '../result-description';
import { DecisionScreenResultDetails } from '../result-details';
import { SaveButton } from '../save-button';

import { styles, INDICATOR_COLORS } from './styles';

const contentBg = require('./decision-bg.png');
const defaultHeaderBg = require('@shared/assets/template-details.mp4');

export const DecisionScreen = function DecisionScreen() {
  const analyticsEventWasCalled = useRef(false);

  const isLoading = useStore($isDecisionSaving);
  const {
    currentDecision: decision,
    optionsTotalSum,
    optionsAllCriteriaValues,
    bestResultName,
    bestResultValue,
    groupedByCriteriaOptionsValues,
  } = useStore($decisionResultStore);

  const optionColors = Object.fromEntries(
    optionsTotalSum.map(([option], index) => [option, INDICATOR_COLORS[index]]),
  );
  const headerBg = decision?.template
    ? { uri: decision.template.video.url }
    : defaultHeaderBg;
  const headerHeight = decision?.template ? undefined : deviceHeight * 0.4;

  const navigation = useNavigation();

  useEffect(() => {
    decisionWasMade();
  }, []);

  const handleButtonPress = async () => {
    await saveDecisionFx(undefined);

    // @ts-expect-error
    navigation.replace('templates');
  };

  const handleAlmostEndViewing = (isVisible: boolean) => {
    if (isVisible && !analyticsEventWasCalled.current) {
      decisionAlmostWhollyViewed();

      analyticsEventWasCalled.current = true;
    }
  };

  if (decision === null) {
    return null;
  }

  return (
    <ScreenWithVideoHeader
      style={styles.container}
      videoSource={headerBg}
      headerHeight={headerHeight}
    >
      <Text
        preset="body_l"
        tx="decision.best"
        style={styles.bestResult_title}
      />
      <Text
        preset="header"
        text={bestResultName}
        style={styles.bestResult__name}
      />

      <Text
        preset="header"
        text={`${bestResultValue}%`}
        style={styles.bestResult__value}
      />

      <DecisionScreenChart
        optionsValues={optionsAllCriteriaValues}
        optionsTotalSum={optionsTotalSum}
        optionColors={optionColors}
      />

      <ImageBackground
        source={contentBg}
        imageStyle={styles.content__image}
        style={styles.content__bg}
        resizeMode="contain"
      >
        <View style={styles.content}>
          <DecisionScreenTotal
            optionColors={optionColors}
            optionsTotalSum={optionsTotalSum}
          />

          <DecisionScreenResultDescription
            bestResultName={bestResultName}
            resultDescription={decision.resultDescription}
          />

          <DecisionScreenResultDetails
            optionColors={optionColors}
            onAlmostEndViewing={handleAlmostEndViewing}
            groupedByCriteriaOptionsValues={groupedByCriteriaOptionsValues}
          />

          <SaveButton
            style={styles.content__button}
            onPress={handleButtonPress}
            isLoading={isLoading}
          />
        </View>
      </ImageBackground>
    </ScreenWithVideoHeader>
  );
};
