import React from 'react';
import { View } from 'react-native';

import { Text } from '@shared/ui';
import { styles } from './styles';

export interface DecisionScreenTotalProps {
  optionColors: Record<string, string>;
  optionsTotalSum: Array<[string, number]>;
}

export const DecisionScreenTotal = function DecisionScreenTotal(
  props: DecisionScreenTotalProps,
) {
  return (
    <View>
      {props.optionsTotalSum.map(([option, result]) => (
        <View key={option} style={styles.total}>
          <Text preset="body_l" style={styles.total__option} text={option} />

          <View
            style={[
              styles.total__indicator,
              { backgroundColor: props.optionColors[option] },
            ]}
          />

          <Text
            preset="body_l"
            style={styles.total__value}
            text={`${result}%`}
          />
        </View>
      ))}
    </View>
  );
};
