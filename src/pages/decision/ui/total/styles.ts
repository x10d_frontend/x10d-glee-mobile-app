import { StyleSheet } from 'react-native';

import { spacing } from '@shared/ui/theme';

export const styles = StyleSheet.create({
  total: {
    alignItems: 'center',
    flexDirection: 'row',
    marginLeft: -spacing[5],
    marginRight: -spacing[5],
    paddingLeft: spacing[5],
    paddingRight: spacing[4],
    paddingVertical: spacing[4],
  },

  total__indicator: {
    borderRadius: spacing[1] / 2,
    height: spacing[1],
    marginLeft: spacing[2],
    width: spacing[1],
  },

  total__option: {
    fontSize: 15,
    fontWeight: '600',
  },

  total__value: {
    fontSize: 19,
    fontWeight: '900',
    marginLeft: 'auto',
  },
});
