import { StyleSheet } from 'react-native';

import { spacing } from '@shared/ui';

export const styles = StyleSheet.create({
  chart: {
    marginTop: spacing[7],
    paddingRight: spacing[7],
  },
  chart__dot: {
    alignItems: 'center',
    borderRadius: spacing[5] / 2,
    height: spacing[5],
    justifyContent: 'center',
    width: spacing[5],
  },
  chart__dotText: {
    fontWeight: '900',
    position: 'absolute',
  },
  chart__legend: {
    marginTop: spacing[2],
  },
});
