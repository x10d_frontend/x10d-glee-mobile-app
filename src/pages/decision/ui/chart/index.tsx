import React from 'react';
import { View } from 'react-native';
import { LineChart } from 'react-native-chart-kit';

import { Text, color, spacing } from '@shared/ui';
import { deviceHeight, deviceWidth } from '@shared/lib/dimensions';

import { styles } from './styles';

export interface DecisionScreenChartProps {
  optionsValues: Array<[string, Array<number>]>;
  optionsTotalSum: Array<[string, number]>;
  optionColors: Record<string, string>;
}

export const DecisionScreenChart = function DecisionChartScreen(
  props: DecisionScreenChartProps,
) {
  const chartDatasets = props.optionsValues
    // Изменяем порядок здесь и ниже, в декораторе, чтобы
    // лучший результат отображался последним и перекрывал
    // остальные линии на графике
    .reverse()
    .map(([option, values]) => ({
      option,
      data: values,
      color: () => props.optionColors[option],
    }));

  // @ts-expect-error
  const decorator = ({ data }) => (
    <View style={styles.chart__legend}>
      {/*@ts-expect-error*/}
      {data.reverse().map((params, index) => (
        <View
          key={params.option}
          style={[
            styles.chart__dot,
            { backgroundColor: params.color() },
            index !== 0 ? { marginTop: spacing[1] } : null,
          ]}
        >
          <Text text={params.option[0]} style={styles.chart__dotText} />
        </View>
      ))}
    </View>
  );

  return (
    <LineChart
      data={{
        labels: [],
        datasets: chartDatasets,
      }}
      decorator={decorator}
      style={styles.chart}
      chartConfig={{
        backgroundGradientFrom: color.palette.jaguar,
        backgroundGradientTo: color.palette.jaguar,
        fillShadowGradientFrom: color.palette.jaguar,
        fillShadowGradientTo: color.palette.jaguar,
        color: () => color.palette.mulledWine,
        propsForBackgroundLines: {
          strokeDasharray: '',
        },
      }}
      width={deviceWidth}
      height={deviceHeight * 0.15}
      withVerticalLabels={false}
      withHorizontalLabels={false}
      withVerticalLines={false}
      withDots={false}
      fromZero
    />
  );
};
