import React from 'react';
import { StyleSheet, View } from 'react-native';
import { BlurView } from 'expo-blur';
import VisibilitySensor from '@svanboxel/visibility-sensor-react-native';

import { MAX_VALUE } from '@entities/decision';

import { Text } from '@shared/ui';

import { styles } from './styles';

export interface DecisionScreenResultDetailedProps {
  optionColors: Record<string, string>;

  onAlmostEndViewing: (isVisible: boolean) => void;

  groupedByCriteriaOptionsValues: Array<[string, Array<[string, number]>]>;
}

export const DecisionScreenResultDetails = function DecisionScreenResultDetails(
  props: DecisionScreenResultDetailedProps,
) {
  const items = props.groupedByCriteriaOptionsValues;

  return (
    <View style={styles.detailedResult}>
      {items.map(([criterion, optionValues], index) => (
        <View key={criterion} style={styles.detailedResult__criterion}>
          <Text
            text={criterion}
            style={styles.detailedResult__criterionTitle}
            preset="body"
          />

          {index === items.length - 2 && (
            <VisibilitySensor onChange={props.onAlmostEndViewing}>
              <View />
            </VisibilitySensor>
          )}

          {optionValues.map(([option, value]) => (
            <View key={option} style={styles.detailedResult__option}>
              <View style={styles.detailedResult__optionHeader}>
                <Text
                  preset="default"
                  text={option}
                  style={styles.detailedResult__optionTitle}
                />

                <Text
                  preset="default"
                  text={value.toString(10)}
                  style={styles.detailedResult__optionValue}
                />
              </View>

              <BlurView
                intensity={20}
                tint="light"
                style={styles.detailedResult__optionBar}
              >
                <View
                  style={[
                    StyleSheet.absoluteFill,
                    {
                      backgroundColor: props.optionColors[option],
                      width: `${(value / MAX_VALUE) * 100}%`,
                    },
                  ]}
                />
              </BlurView>
            </View>
          ))}
        </View>
      ))}
    </View>
  );
};
