import { StyleSheet } from 'react-native';

import { color, spacing } from '@shared/ui/theme';

export const styles = StyleSheet.create({
  detailedResult: {
    marginTop: spacing[7],
  },

  detailedResult__criterion: {
    marginTop: spacing[7],
  },

  detailedResult__criterionTitle: {
    fontWeight: '900',
    marginBottom: spacing[4],
  },

  detailedResult__option: {
    borderBottomColor: color.palette.bleachedCedar,
    borderBottomWidth: 1,
    paddingVertical: spacing[4],
  },

  detailedResult__optionBar: {
    borderRadius: 5,
    height: 10,
    overflow: 'hidden',
  },

  detailedResult__optionHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: spacing[3],
  },

  detailedResult__optionTitle: {
    fontWeight: '600',
    lineHeight: 18,
  },

  detailedResult__optionValue: {
    fontWeight: '900',
    lineHeight: 18,
  },
});
