import React from 'react';
import { Text as RNText } from 'react-native';
import { BlurView } from 'expo-blur';

import { Text } from '@shared/ui';

import { styles } from './styles';

interface IDecisionScreenResultDescriptionProps {
  bestResultName?: string;
  resultDescription: string;
}
export const DecisionScreenResultDescription =
  function DecisionScreenResultDescription(
    props: IDecisionScreenResultDescriptionProps,
  ) {
    return (
      <BlurView tint="dark" style={styles.resultDescription}>
        <Text preset="body" style={styles.resultDescription__text}>
          {/* Пробел после значения важен */}
          <RNText style={styles.resultDescription__text_resultName}>
            {props.bestResultName}{' '}
          </RNText>

          <RNText>{props.resultDescription}</RNText>
        </Text>
      </BlurView>
    );
  };
