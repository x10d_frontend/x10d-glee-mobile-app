import { StyleSheet } from 'react-native';

import { color, spacing } from '@shared/ui';

export const styles = StyleSheet.create({
  resultDescription: {
    borderRadius: 20,
    marginTop: spacing[8],
    padding: spacing[5],
    overflow: 'hidden',
  },

  resultDescription__text: {
    fontSize: 24,
    fontWeight: '900',
    lineHeight: 28,
  },

  resultDescription__text_resultName: {
    color: color.palette.heliotrope,
  },
});
