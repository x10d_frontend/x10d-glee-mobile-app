import React from 'react';
import { ActivityIndicator, StyleProp, ViewStyle } from 'react-native';

import { Button, color } from '@shared/ui';

interface ISaveButtonProps {
  style: StyleProp<ViewStyle>;

  onPress: () => void;

  isLoading: boolean;
}

export const SaveButton = React.memo(
  (props: ISaveButtonProps) => {
    return (
      <Button
        style={props.style}
        onPress={props.onPress}
        disabled={props.isLoading}
        tx={!props.isLoading ? 'buttons.complete' : undefined}
        preset="dark"
      >
        {props.isLoading && <ActivityIndicator color={color.palette.white} />}
      </Button>
    );
  },
  (prev, next) => prev.isLoading === next.isLoading,
);
