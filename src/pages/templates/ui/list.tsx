import React from 'react';
import ContentLoader, { Rect } from 'react-content-loader/native';
import { useStore } from 'effector-react';
import { range } from 'ramda';

import {
  $filteredTemplates,
  TemplateCard,
  TemplateCardSkeleton,
} from '@entities/template';

import { color, spacing } from '@shared/ui';

export const TemplatesList = function TemplatesList() {
  const filteredTemplates = useStore($filteredTemplates);

  return (
    <>
      {filteredTemplates.map((template, index) => (
        <TemplateCard
          key={template.id}
          item={template}
          style={index === 0 ? { marginTop: spacing[6] } : null}
        />
      ))}
    </>
  );
};

export const TemplatesScreenSkeleton: React.FC = () => (
  <ContentLoader
    backgroundColor={color.palette.mulledWine}
    foregroundColor={color.palette.mulledWineDarken}
  >
    <Rect width={130} height={22} rx={5} ry={5} />

    <Rect width={230} height={37} rx={5} ry={5} y={33} />

    <Rect width={170} height={17} rx={5} ry={5} y={88} />
    <Rect width={90} height={17} rx={5} ry={5} y={108} />

    <Rect width={62} height={32} rx={8} ry={8} y={173} />
    <Rect width={102} height={32} rx={8} ry={8} x={70} y={173} />
    <Rect width={82} height={32} rx={8} ry={8} x={180} y={173} />

    {range(0, 3).map(index => (
      <TemplateCardSkeleton
        key={index}
        style={index === 0 ? { marginTop: 212 + spacing[6] } : null}
      />
    ))}
  </ContentLoader>
);
