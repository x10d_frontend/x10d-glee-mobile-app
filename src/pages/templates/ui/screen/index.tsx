import React, { useEffect } from 'react';
import { View } from 'react-native';
import { useStore } from 'effector-react';

import {
  FeaturedTemplate,
  $featuredTemplate,
} from '@features/featured-template';
import { TemplatesFilters } from '@features/template-filters';

import { $isLoading, fetchTemplates } from '@entities/template';

import { ScreenWithVideoHeader } from '@shared/ui';

import { CustomTemplate } from '../custom-template';
import { TemplatesList, TemplatesScreenSkeleton } from '../list';

import { styles } from './styles';

export const TemplatesScreen = function TemplatesScreen() {
  const isLoading = useStore($isLoading);
  const featuredTemplate = useStore($featuredTemplate);

  const videoSource = featuredTemplate
    ? { uri: featuredTemplate.video.url }
    : undefined;

  useEffect(() => {
    fetchTemplates();
  }, []);

  return (
    <ScreenWithVideoHeader
      videoSource={videoSource}
      contentStyle={styles.screen__contentContainer}
    >
      <View style={styles.screen__content}>
        {isLoading ? (
          <TemplatesScreenSkeleton />
        ) : (
          <>
            <FeaturedTemplate />

            <TemplatesFilters style={styles.screen__filters} />

            <TemplatesList />

            <CustomTemplate style={styles.screen__customTemplate} />
          </>
        )}
      </View>
    </ScreenWithVideoHeader>
  );
};
