import { StyleSheet } from 'react-native';

import { screenFirstElementOffset, spacing } from '@shared/ui/theme';

export const styles = StyleSheet.create({
  screen__content: {
    marginTop: screenFirstElementOffset,
  },

  screen__contentContainer: {
    paddingBottom: 0,
  },

  screen__customTemplate: {
    marginTop: spacing[5],
  },

  screen__filters: {
    marginTop: spacing[7],
  },
});
