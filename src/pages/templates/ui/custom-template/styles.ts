import { StyleSheet } from 'react-native';

import { mainSidePadding, spacing } from '@shared/ui/theme';
import { deviceHeight, deviceWidth } from '@shared/lib/dimensions';

export const styles = StyleSheet.create({
  container: {
    marginHorizontal: -mainSidePadding,
  },
  container__bg: {
    height: deviceHeight * 0.4,
    width: deviceWidth,
  },
  content: {
    ...StyleSheet.absoluteFillObject,
    alignItems: 'center',
  },
  contentButton__text: {
    fontSize: 16,
    lineHeight: 25,
  },
  content__button: {
    height: spacing[7],
    marginTop: spacing[5],
    width: deviceWidth * 0.5,
  },
  content__title: {
    fontSize: 25,
    marginTop: spacing[8],
    maxWidth: deviceWidth * 0.7,
    textAlign: 'center',
  },
});
