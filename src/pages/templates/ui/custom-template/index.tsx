import React from 'react';
import { StyleProp, View, ViewStyle } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { ResizeMode, Video } from 'expo-av';
import { useStore } from 'effector-react';

import { createDecision } from '@entities/decision';
import { $viewer } from '@entities/viewer';

import { Button, GradientBackground, Text } from '@shared/ui';

import { styles } from './styles';

const bg = require('@shared/assets/template-details.mp4');

interface TemplatesScreenCustomTemplateProps {
  style: StyleProp<ViewStyle>;
}

export const CustomTemplate = function CustomTemplate(
  props: TemplatesScreenCustomTemplateProps,
) {
  const viewer = useStore($viewer);
  const navigation = useNavigation();

  const handleButtonClick = () => {
    createDecision({
      viewer,
    });

    // @ts-expect-error
    navigation.navigate('decisionStack');
  };

  return (
    <View style={[styles.container, props.style]}>
      <Video
        source={bg}
        style={styles.container__bg}
        resizeMode={ResizeMode.COVER}
        shouldPlay
        isLooping
      />

      <GradientBackground
        locations={[0, 0.7]}
        colors={['#090110', 'rgba(9, 1, 16, 0)']}
      />

      <View style={styles.content}>
        <Text
          preset="header"
          tx="template.customTitle"
          style={styles.content__title}
        />

        <Button
          tx="buttons.start"
          style={styles.content__button}
          textStyle={styles.contentButton__text}
          onPress={handleButtonClick}
        />
      </View>
    </View>
  );
};
