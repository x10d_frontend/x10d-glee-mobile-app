import React from 'react';
import { StyleProp, View, ViewStyle, StyleSheet } from 'react-native';
import { useStore } from 'effector-react';

import { IDecision, $finalValue } from '@entities/decision';

import { TemplateCriteriaValuesListItem } from './list-item';

export interface TemplateCriteriaValuesListProps {
  decision: IDecision;

  onChangeValue: (value: number, option: string, criterion: string) => void;

  style?: StyleProp<ViewStyle>;
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
  },
});

export const TemplateCriteriaValuesList = function TemplateCriteriaValuesList(
  props: TemplateCriteriaValuesListProps,
) {
  const decisionValue = useStore($finalValue);

  return (
    <View style={[styles.container, props.style]}>
      {props.decision.criteria.map(criterion => (
        <TemplateCriteriaValuesListItem
          key={criterion}
          decision={props.decision}
          decisionValue={decisionValue}
          criterion={criterion}
          onChangeValue={props.onChangeValue}
        />
      ))}
    </View>
  );
};
