import React from 'react';
import { StyleSheet, View } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { useNavigation } from '@react-navigation/native';
import { useStore } from 'effector-react';

import {
  $currentDecisionStore,
  goToNextStep,
  goToPreviousStep,
  setCriterionWeight,
} from '@entities/decision';

import {
  Button,
  GradientBackground,
  Icon,
  ScreenWithVideoHeader,
  Text,
  spacing,
  mainSidePadding,
  mainButtonHeight,
  screenFirstElementOffset,
} from '@shared/ui';
import { deviceHeight } from '@shared/lib/dimensions';

import { TemplateCriteriaValuesList } from './list';

const templateDetails = require('@shared/assets/template-details.mp4');

const styles = StyleSheet.create({
  screenButton__icon_back: {
    justifyContent: 'center',
  },

  screenButton__icon_next: {
    transform: [{ rotate: '180deg' }],
  },

  screen__button_back: {
    marginRight: spacing[3],
  },

  screen__button_next: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  screen__buttons: {
    flexDirection: 'row',
    left: mainSidePadding,
    position: 'absolute',
    right: mainSidePadding,
  },

  screen__description: {
    flex: 1,
    marginTop: spacing[2],
  },

  screen__list: {
    marginBottom: mainButtonHeight + spacing[5],
    marginTop: spacing[6],
  },

  screen__title: {
    marginTop: screenFirstElementOffset,
  },
});

export const TemplateCriteriaValuesScreen =
  function TemplateCriteriaValuesScreen() {
    const { currentDecision: decision, wasValueChanged } = useStore(
      $currentDecisionStore,
    );

    const { bottom: bottomInset } = useSafeAreaInsets();
    const navigation = useNavigation();

    const isNextButtonDisabled = !wasValueChanged;
    const currentStep = navigation.getState().index + 1;

    const handleValueChanging = (
      value: number,
      option: string,
      criterion: string,
    ) => {
      setCriterionWeight({
        step: currentStep,
        value,
        option,
        criterion,
      });
    };
    const handleNextButtonPress = () => {
      goToNextStep(currentStep);

      navigation.reset({
        index: 0,
        // @ts-expect-error
        routes: [{ name: 'decisionResult' }],
      });
    };

    const handleBackButtonPress = () => {
      goToPreviousStep(currentStep);

      navigation.goBack();
    };

    if (!decision) {
      return null;
    }

    return (
      <>
        <ScreenWithVideoHeader
          videoSource={templateDetails}
          headerHeight={deviceHeight * 0.4}
        >
          <Text
            style={styles.screen__title}
            tx="template.criteriaValuesTitle"
            preset="header"
          />
          <Text
            style={styles.screen__description}
            tx="template.criteriaValuesDescription"
            preset="body_l"
          />

          <TemplateCriteriaValuesList
            decision={decision}
            style={styles.screen__list}
            onChangeValue={handleValueChanging}
          />
        </ScreenWithVideoHeader>

        <GradientBackground
          pointerEvents="none"
          locations={[0.8, 1]}
          colors={['#09011000', '#090110']}
        />

        <View style={[styles.screen__buttons, { bottom: bottomInset }]}>
          <Button
            style={styles.screen__button_back}
            onPress={handleBackButtonPress}
            preset="dark_rounded"
          >
            <Icon
              icon="chevron"
              containerStyle={styles.screenButton__icon_back}
            />
          </Button>

          <Button
            disabled={isNextButtonDisabled}
            style={styles.screen__button_next}
            tx="buttons.next"
            onPress={handleNextButtonPress}
            preset="pink"
          >
            <Icon icon="chevron" style={styles.screenButton__icon_next} />
          </Button>
        </View>
      </>
    );
  };
