import * as React from 'react';
import { StyleSheet, View } from 'react-native';
import ReactNativeHapticFeedback from 'react-native-haptic-feedback';

import { IDecision } from '@entities/decision';

import { Text, InputNumber, spacing, color } from '@shared/ui';
import { deviceWidth } from '@shared/lib/dimensions';

export interface TemplateCriteriaValuesListItemProps {
  decision: IDecision;

  decisionValue: IDecision['value'];

  criterion: string;

  onChangeValue: (value: number, option: string, criterion: string) => void;
}

const styles = StyleSheet.create({
  container: {
    borderBottomColor: color.palette.bastille,
    borderBottomWidth: 1,
    paddingBottom: spacing[4],
    paddingTop: spacing[5],
  },

  option: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: spacing[2],
  },

  option__text: {
    maxWidth: deviceWidth * 0.4,
  },

  title: {
    fontSize: 19,
    fontWeight: '900',
    marginBottom: spacing[4],
  },
});

export const TemplateCriteriaValuesListItem = React.memo(
  (props: TemplateCriteriaValuesListItemProps) => {
    const handleValueChanging = (value: number, option: string) => {
      ReactNativeHapticFeedback.trigger('impactLight');

      props.onChangeValue(value, option, props.criterion);
    };

    return (
      <View style={styles.container}>
        <Text preset="body_l" text={props.criterion} style={styles.title} />

        {props.decision.options.map(option => (
          <View key={option} style={styles.option}>
            <Text
              preset="body_l"
              style={styles.option__text}
              text={option}
              ellipsizeMode="tail"
              numberOfLines={1}
            />

            <InputNumber
              value={props.decisionValue[option][props.criterion]}
              onChangeValue={value => handleValueChanging(value, option)}
            />
          </View>
        ))}
      </View>
    );
  },
  ({ decisionValue: prev, criterion }, { decisionValue: next }) => {
    const prevCriterionSum = Object.values(prev).reduce(
      (acc, value) => acc + value[criterion],
      0,
    );
    const nextCriterionSum = Object.values(next).reduce(
      (acc, value) => acc + value[criterion],
      0,
    );

    return prevCriterionSum === nextCriterionSum;
  },
);
