import React, { useEffect } from 'react';
import { Linking, Pressable } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { useStore } from 'effector-react';
import { useNavigation, useRoute } from '@react-navigation/native';

import {
  $templatesMap,
  $templateTagsMap,
  templateDetailWasClosed,
  templateVideoAuthorPressed,
} from '@entities/template';
import { createDecision } from '@entities/decision';
import { $viewer } from '@entities/viewer';

import { translateEntity } from '@shared/lib/i18n';
import {
  ScreenWithVideoHeader,
  Text,
  Button,
  GradientBackground,
  Icon,
  mainSidePadding,
  mainButtonHeight,
} from '@shared/ui';

import { styles } from './styles';

export const TemplatePreviewScreen = function TemplatePreviewScreen() {
  const templatesMap = useStore($templatesMap);
  const templateTagsMap = useStore($templateTagsMap);
  const viewer = useStore($viewer);

  const { bottom: bottomInset } = useSafeAreaInsets();
  const navigation = useNavigation();
  const route = useRoute();
  // @ts-expect-error
  const { templateId } = route.params;

  const template = templatesMap[templateId];

  useEffect(() =>
    navigation.addListener('beforeRemove', () => {
      templateDetailWasClosed(template);
    }),
  );

  if (!template) {
    return null;
  }

  const handleButtonPress = () => {
    createDecision({
      template,
      viewer,
    });

    // @ts-expect-error
    navigation.navigate('decisionStack');
  };

  const handleLinkPress = async () => {
    if (template.video.authorLink !== undefined) {
      templateVideoAuthorPressed(template);

      await Linking.openURL(template.video.authorLink);
    }
  };

  return (
    <>
      <ScreenWithVideoHeader
        style={{
          paddingBottom: mainButtonHeight + mainSidePadding,
        }}
        videoSource={{ uri: template.video.url }}
      >
        <Text
          preset="header"
          text={translateEntity(template.title)}
          style={styles.header}
        />

        {template.tags.map(tag => (
          <Text
            preset="subHeader"
            key={tag}
            text={translateEntity(templateTagsMap[tag].title)}
            style={styles.subHeader}
          />
        ))}

        <Text
          preset="body"
          text={translateEntity(template.description)}
          style={styles.bodyText}
        />

        {template.video.authorName !== undefined &&
          template.video.authorLink !== undefined && (
            <Pressable
              style={styles.image__authorLink}
              onPress={handleLinkPress}
            >
              <Text tx="videoBy" preset="secondary" />

              <Icon icon="chain" style={styles.imageAuthorLink__icon} />

              <Text
                text={template.video.authorName}
                preset="secondary"
                style={styles.imageAuthorLink__text}
              />
            </Pressable>
          )}
      </ScreenWithVideoHeader>

      <GradientBackground
        pointerEvents="none"
        locations={[0.8, 1]}
        colors={['#09011000', '#090110']}
      />

      <Button
        style={{
          ...styles.button,
          bottom: bottomInset,
        }}
        onPress={handleButtonPress}
        tx="buttons.start"
      />
    </>
  );
};
