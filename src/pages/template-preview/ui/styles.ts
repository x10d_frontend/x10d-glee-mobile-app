import { StyleSheet } from 'react-native';

import { color, mainSidePadding, spacing } from '@shared/ui';
import { deviceWidth } from '@shared/lib/dimensions';

export const styles = StyleSheet.create({
  bodyText: {
    flex: 1,
    marginTop: spacing[7],
    width: deviceWidth * 0.7,
  },
  button: {
    bottom: 0,
    left: mainSidePadding,
    position: 'absolute',
    right: mainSidePadding,
  },
  header: {
    marginTop: -spacing[6],
    maxWidth: deviceWidth * 0.7,
  },
  imageAuthorLink__icon: {
    marginLeft: spacing[2],
  },
  imageAuthorLink__text: {
    color: color.link,
    marginLeft: spacing[2],
  },
  image__authorLink: {
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: spacing[6],
  },
  subHeader: {
    marginTop: spacing[2],
  },
});
