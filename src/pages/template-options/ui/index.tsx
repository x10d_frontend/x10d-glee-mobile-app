import React from 'react';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { useNavigation } from '@react-navigation/native';
import { useStore } from 'effector-react';

import {
  $currentDecisionStore,
  goToNextStep,
  addOptionPressed,
  addOption,
  deleteOption,
  editOption,
} from '@entities/decision';

import {
  ScreenWithVideoHeader,
  Text,
  EditableList,
  Button,
  Icon,
  color,
} from '@shared/ui';
import { deviceHeight } from '@shared/lib/dimensions';

import { styles } from './styles';

const templateDetails = require('@shared/assets/template-details.mp4');

export const TemplateOptionsScreen = function TemplateOptionsScreen() {
  const {
    currentDecision: decision,
    isOptionsCountValid,
    isAddingOptionAllowed,
  } = useStore($currentDecisionStore);

  const navigation = useNavigation();
  const { bottom: bottomInset } = useSafeAreaInsets();

  const isNextButtonDisabled = !isOptionsCountValid;
  const currentStep = navigation.getState().index + 1;

  const handleNextButtonPress = () => {
    goToNextStep(currentStep);

    // @ts-expect-error
    navigation.navigate('decisionCriteria');
  };
  const handleAddButtonPress = () => {
    addOptionPressed(currentStep);
  };
  const handleAddingOption = (option: string) => {
    addOption({
      step: currentStep,
      option,
    });
  };
  const handleDeletingOption = (option: string) => {
    deleteOption({
      step: currentStep,
      option,
    });
  };
  const handleEditingOption = (option: string) => {
    editOption({
      step: currentStep,
      option,
    });
  };

  if (decision === null) {
    return null;
  }

  return (
    <>
      <ScreenWithVideoHeader
        videoSource={templateDetails}
        headerHeight={deviceHeight * 0.4}
      >
        <Text
          style={styles.screen__title}
          text={decision.optionsTitle}
          preset="header"
        />

        <Text
          style={styles.screen__description}
          text={decision.optionsDescription}
          preset="body_l"
        />

        <EditableList
          items={decision.options}
          onAddItem={handleAddingOption}
          onEditItem={handleEditingOption}
          onDeleteItem={handleDeletingOption}
          onAddButtonPress={handleAddButtonPress}
          isAddingItemsAllowed={isAddingOptionAllowed}
          style={styles.screen_optionsList}
        />
      </ScreenWithVideoHeader>

      <Button
        style={[styles.screen__button, { bottom: bottomInset }]}
        tx="buttons.next"
        disabled={isNextButtonDisabled}
        onPress={handleNextButtonPress}
        preset="pink"
      >
        <Icon
          icon="chevron"
          style={[
            styles.screenButton__icon,
            isNextButtonDisabled
              ? { tintColor: color.palette.mulledWine }
              : null,
          ]}
        />
      </Button>
    </>
  );
};
