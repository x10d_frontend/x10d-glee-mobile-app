import { StyleSheet } from 'react-native';

import { mainSidePadding, screenFirstElementOffset, spacing } from '@shared/ui';

export const styles = StyleSheet.create({
  screenButton__icon: {
    transform: [{ rotate: '180deg' }],
  },

  screen__button: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    left: mainSidePadding,
    position: 'absolute',
    right: mainSidePadding,
  },

  screen__description: {
    flex: 1,
    marginTop: spacing[2],
  },

  screen__title: {
    marginTop: screenFirstElementOffset,
  },

  screen_optionsList: {
    marginTop: spacing[6],
  },
});
